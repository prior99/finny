default: build lint test

.PHONY: node_modules
node_modules:
	yarn install
	yarn lerna bootstrap

.PHONY: build
build: node_modules
	yarn lerna run build

.PHONY: lint
lint: node_modules
	yarn lint

.PHONY: clean
clean:
	yarn lerna run clean

.PHONY: run
run: node_modules db
	yarn lerna run --parallel start

.PHONY: db
db:
	createdb finny || true

.PHONY: clean-db
clean-db:
	dropdb finny || true
