import { component, factory, inject, destroy, initialize } from "tsdi";
import { Connection } from "typeorm";
import { info, error } from "winston";
import { Config } from "../config";
import { connect } from "../database";

@component({ eager: true })
export class DatabaseFactory {
    @inject private config: Config;

    public connection: Connection;

    @initialize
    public async connect() {
        this.connection = await connect(this.config);
    }

    @factory
    public getConnection(): Connection { return this.connection; }

    @destroy
    public async stop() {
        if (this.connection && this.connection.isConnected) {
            try {
                await this.connection.close();
            } catch (err) {
                error(`Error occured when closing databsae connection: ${err.message}`);
            }
            info("Disconnected from database.");
        }
    }
}
