import { inject, external } from "tsdi";
import { Request } from "express";
import { Connection } from "typeorm";
import { ControllerValidation, User, Context } from "finny-common";
import { getAuthTokenId } from "./auth-token";

@external
export class ServerContext implements Context {
    @inject public validation: ControllerValidation;
    @inject private db: Connection;

    private authTokenId: string;

    constructor(req: Request) {
        this.authTokenId = getAuthTokenId(req);
    }

    public async currentUser() {
        const id = this.authTokenId;
        if (!id) { return; }
        return await this.db.getRepository(User).createQueryBuilder("user")
            .innerJoin("user.tokens", "token")
            .where("token.id=:id", { id })
            .getOne();
    }

    public async isUser(id: string): Promise<boolean> {
        return (await this.currentUser()).id === id;
    }
}
