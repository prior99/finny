import { MigrationInterface, QueryRunner } from "typeorm";
import { info } from "winston";

export class Transactions1536566697290 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        info("Applying migration: Transactions");
        await queryRunner.query(`
            CREATE TABLE "sepa_account" (
                id UUID NOT NULL DEFAULT uuid_generate_v4(),
                iban CHARACTER VARYING (40) NOT NULL,
                bic CHARACTER VARYING (16) NOT NULL,
                account_number CHARACTER VARYING NOT NULL,
                sub_account CHARACTER VARYING NOT NULL,
                fin_ts_credentials_id UUID NOT NULL,
                CONSTRAINT pk_sepa_account_id PRIMARY KEY (id),
                CONSTRAINT fk_fin_ts_credentials_id
                    FOREIGN KEY (fin_ts_credentials_id)
                    REFERENCES fin_ts_credentials (id)
            )
        `);
        await queryRunner.query(`
            CREATE TABLE "statement" (
                id UUID NOT NULL DEFAULT uuid_generate_v4(),
                reference_number CHARACTER VARYING NOT NULL,
                related_reference_number CHARACTER VARYING,
                account_id CHARACTER VARYING NOT NULL,
                number CHARACTER VARYING NOT NULL,
                opening_balance DECIMAL NOT NULL,
                opening_date TIMESTAMP NOT NULL,
                closing_balance DECIMAL NOT NULL,
                closing_date TIMESTAMP NOT NULL,
                sepa_account_id UUID NOT NULL,
                CONSTRAINT pk_statement_id PRIMARY KEY (id),
                CONSTRAINT fk_sepa_account_id FOREIGN KEY (sepa_account_id) REFERENCES sepa_account (id)
            )
        `);
        await queryRunner.query(`
            CREATE TABLE "transaction" (
                id uuid NOT NULL DEFAULT uuid_generate_v4(),
                code CHARACTER VARYING NOT NULL,
                funds_code CHARACTER VARYING NOT NULL,
                currency CHARACTER VARYING NOT NULL,
                description TEXT NOT NULL,
                amount DECIMAL NOT NULL,
                value_date TIMESTAMP NOT NULL,
                entry_date TIMESTAMP NOT NULL,
                customer_reference CHARACTER VARYING NOT NULL,
                bank_reference CHARACTER VARYING NOT NULL,
                name CHARACTER VARYING,
                iban CHARACTER VARYING (40),
                text TEXT,
                bic CHARACTER VARYING (16),
                prima_nota CHARACTER VARYING,
                reference JSONB,
                statement_id UUID NOT NULL,
                CONSTRAINT pk_transaction_id PRIMARY KEY (id),
                CONSTRAINT fk_statement_id FOREIGN KEY (statement_id) REFERENCES statement(id)
            )
        `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        info("Reverting migration: Transactions");
        await queryRunner.query(`DROP TABLE transaction`);
        await queryRunner.query(`DROP TABLE statement`);
        await queryRunner.query(`DROP TABLE sepa_account`);
    }
}
