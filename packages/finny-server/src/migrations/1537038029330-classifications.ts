import { MigrationInterface, QueryRunner } from "typeorm";
import { info } from "winston";

export class Classifications1537038029330 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        info("Applying migration: Transactions");
        await queryRunner.query(`
            CREATE TABLE "tag" (
                id UUID NOT NULL DEFAULT uuid_generate_v4(),
                name CHARACTER VARYING NOT NULL,
                parent_id UUID,
                user_id UUID NOT NULL,
                CONSTRAINT pk_tag_id PRIMARY KEY (id),
                CONSTRAINT fk_user_id
                    FOREIGN KEY (user_id)
                    REFERENCES user (id),
                CONSTRAINT fk_parent_id
                    FOREIGN KEY (parent_id)
                    REFERENCES tag (id)
            )
        `);
        await queryRunner.query(`
            CREATE TABLE "classification" (
                id UUID NOT NULL DEFAULT uuid_generate_v4(),
                tag_id UUID NOT NULL,
                transaction_id UUID NOT NULL,
                classifier_id UUID,
                CONSTRAINT pk_classification_id PRIMARY KEY (id),
                CONSTRAINT fk_classifier_id
                    FOREIGN KEY (classifier_id)
                    REFERENCES classifier (id),
                CONSTRAINT fk_tag_id
                    FOREIGN KEY (tag_id)
                    REFERENCES tag (id),
                CONSTRAINT fk_transaction_id
                    FOREIGN KEY (transaction_id)
                    REFERENCES transaction (id)
            )
        `);
        await queryRunner.query(`
            CREATE TABLE "classifier" (
                id UUID NOT NULL DEFAULT uuid_generate_v4(),
                tag_id UUID NOT NULL,
                iban CHARACTER VARYING (40),
                name_regexp CHARACTER VARYING,
                text_regexp CHARACTER VARYING,
                reference_regexp CHARACTER VARYING,
                threshold_min DECIMAL,
                threshold_max DECIMAL,
                CONSTRAINT pk_classifier_id PRIMARY KEY (id),
                CONSTRAINT fk_tag_id
                    FOREIGN KEY (tag_id)
                    REFERENCES tag (id)
            )
        `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        info("Reverting migration: Transactions");
        await queryRunner.query(`DROP TABLE tag`);
        await queryRunner.query(`DROP TABLE classification`);
        await queryRunner.query(`DROP TABLE classifier`);
    }
}
