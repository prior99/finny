import { MigrationInterface, QueryRunner } from "typeorm";
import { info } from "winston";

export class Initial1536395873419 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        info("Applying migration: Initial");
        await queryRunner.query(`
            CREATE TABLE "user" (
                id uuid NOT NULL DEFAULT uuid_generate_v4(),
                name character varying(100) NOT NULL,
                password character varying(200) NOT NULL,
                email character varying(200) NOT NULL,
                enabled boolean NOT NULL DEFAULT false,
                admin boolean NOT NULL DEFAULT false,
                CONSTRAINT pk_user_id PRIMARY KEY (id)
            )
        `);
        await queryRunner.query(`
            CREATE TABLE "token" (
                id uuid NOT NULL DEFAULT uuid_generate_v4(),
                created TIMESTAMP NOT NULL DEFAULT now(),
                updated TIMESTAMP NOT NULL DEFAULT now(),
                deleted TIMESTAMP WITH TIME ZONE,
                user_id uuid,
                CONSTRAINT pk_token_id PRIMARY KEY (id),
                CONSTRAINT fk_user_id FOREIGN KEY (user_id) REFERENCES "user"(id)
            )
        `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        info("Reverting migration: Initial");
        await queryRunner.query(`DROP TABLE token`);
        await queryRunner.query(`DROP TABLE user`);
    }
}
