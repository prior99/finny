import { Initial1536395873419 } from "./1536395873419-initial";
import { FinTSCredentials1536561553702 } from "./1536561553702-fints-credentials";
import { Transactions1536566697290 } from "./1536566697290-transactions";
import { Classifications1537038029330 } from "./1537038029330-classifications";

export const migrations = [
    Initial1536395873419,
    FinTSCredentials1536561553702,
    Transactions1536566697290,
    Classifications1537038029330,
];
