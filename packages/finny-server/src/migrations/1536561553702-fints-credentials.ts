import { MigrationInterface, QueryRunner } from "typeorm";
import { info } from "winston";

export class FinTSCredentials1536561553702 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        info("Applying migration: FinTS credentials");
        await queryRunner.query(`
            CREATE TABLE "fin_ts_credentials" (
                id uuid NOT NULL DEFAULT uuid_generate_v4(),
                name character varying(100) NOT NULL,
                pin character varying(100) NOT NULL,
                url character varying(200) NOT NULL,
                blz character (8) NOT NULL,
                user_id uuid NOT NULL,
                CONSTRAINT pk_fin_ts_credentials_id PRIMARY KEY (id),
                CONSTRAINT fk_user_id FOREIGN KEY (user_id) REFERENCES "user"(id)
            )
        `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        info("Reverting migration: FinTS credentials");
        await queryRunner.query(`DROP TABLE fints_credentials`);
    }
}
