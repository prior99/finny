import { Request } from "express";
import { Context } from "finny-common";
import { getAuthTokenId } from "./auth-token";

export async function checkAuthorization(request: Request, context: Context) {
    const id = getAuthTokenId(request);
    return typeof (await context.validation.tokenValid(id)).error === "undefined";
}
