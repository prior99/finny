import * as Express from "express";
import * as BodyParser from "body-parser";
import { info } from "winston";
import { hyrest } from "hyrest-express";
import { AuthorizationMode } from "hyrest";
import { component, initialize, inject, TSDI } from "tsdi";
import * as morgan from "morgan";
import { controllers } from "finny-common";
import { cors } from "./middlewares";
import { Config } from "./config";
import { ServerContext } from "./server-context";
import { checkAuthorization } from "./check-authorization";

@component({ eager: true })
export class Api {
    @inject private tsdi: TSDI;
    @inject private config: Config;

    private app: Express.Application;

    @initialize
    public initialize() {
        this.app = Express();
        this.app.use(morgan("tiny", { stream: { write: msg => info(msg.trim()) } }));
        this.app.use(BodyParser.json());
        this.app.use(BodyParser.urlencoded({ extended: true }));
        this.app.use(cors);

        const controllerInstances = controllers.map(controllerClass => this.tsdi.get(controllerClass));
        const hyrestMiddleware = hyrest(...controllerInstances)
            .context(async request => new ServerContext(request))
            .defaultAuthorizationMode(AuthorizationMode.AUTH)
            .authorization(checkAuthorization);
        this.app.use(hyrestMiddleware);

        this.app.listen(this.config.port);
        info(`Server listening on port ${this.config.port}.`);
    }
}
