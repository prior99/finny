import { metadata, command, Command } from "clime";
import { Config } from "../config";
import { connect } from "../database";

@command({ description: "Perform necessary database migrations" })
export default class extends Command { // tslint:disable-line
    @metadata
    public async execute(config: Config) {
        config.load();
        config.dbLogging = true;
        const db = await connect(config);
        await db.runMigrations();
        await db.close();
    }
}
