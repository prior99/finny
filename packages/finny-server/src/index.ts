#!/usr/bin/env node
import * as path from "path";
import * as Winston from "winston";
import { CLI, Shim } from "clime";
import { error } from "winston";
import { logger as fintsLogger } from "fints";

function createTransport(level: string) {
    return new Winston.transports.Console({
        format: Winston.format.combine(
            Winston.format.timestamp({ format: "YYYY-MM-DDTHH:mm:ss" }),
            Winston.format.colorize(),
            Winston.format.printf(info => `${info.timestamp} - ${info.level}: ${info.message}`),
        ),
        level,
    });
}

Winston.remove(Winston.transports.Console);
Winston.add(createTransport("verbose"));

fintsLogger.add(createTransport("verbose"));
fintsLogger.silent = true;

process.on("unhandledRejection", err => {
    error(`Unhandled Promise rejection: ${err.message}`);
    console.error(err);
});
process.on("uncaughtException", err => {
    error(`Unhandled Promise rejection: ${err.message}`);
    console.error(err);
});

const cli = new CLI("fints", path.join(__dirname, "commands"));
const shim = new Shim(cli);
shim.execute(process.argv);
