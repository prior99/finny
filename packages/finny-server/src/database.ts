import { createConnection } from "typeorm";
import { info } from "winston";
import {
    Classification,
    Classifier,
    FinTSCredentials,
    SEPAAccount,
    Statement,
    Tag,
    Token,
    Transaction,
    User,
} from "finny-common";
import { Config } from "./config";
import { SnakeNamingStrategy } from "./snake-naming-strategy";
import { migrations } from "./migrations";

const entities = [
    Classification,
    Classifier,
    FinTSCredentials,
    SEPAAccount,
    Statement,
    Tag,
    Token,
    Transaction,
    User,
];

export async function connect(config: Config) {
    const {
        dbName: database,
        dbLogging: logging,
        dbPassword: password,
        dbPort: port,
        dbUsername: username,
        dbHost: host,
    } = config;
    const type = "postgres";
    const namingStrategy = new SnakeNamingStrategy();
    const connection = await createConnection({
        entities, database, type, logging, password, port, username, host, migrations, namingStrategy,
    });
    info("Connected to database.");
    return connection;
}
