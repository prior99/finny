const path = require('path');
const ExtractTextPlugin = require("extract-text-webpack-plugin");

const extractCSS = new ExtractTextPlugin('bundle.css');

module.exports = {
    mode: "development",
    entry:  path.join(__dirname, "src"),
    output: {
        path: path.join(__dirname, "dist"),
        filename: "bundle.js",
        publicPath: "/dist"
    },
    resolve: {
        extensions: [".js", ".ts", ".tsx"],
        alias: {
            "typeorm": "typeorm/browser",
        }
    },
    module: {
        rules: [
            {
                test: /\.(woff|woff2)$/,
                loader: "url-loader",
            }, {
                test: /\.(png|svg|ttf|eot)$/,
                loader: "file-loader",
            }, {
                test: /\.tsx?/,
                loader: "ts-loader",
            }, {
                test: /\.css$/,
                loader: extractCSS.extract({
                    use: [
                        {
                            loader: "css-loader",
                            options: { sourceMap: true },
                        }, {
                            loader: "resolve-url-loader",
                        },
                    ],
                }),
            }, {
                test: /\.scss$/,
                loader: extractCSS.extract({
                    use: [
                        {
                            loader: "css-loader",
                            options: {
                                modules: true,
                                importLoaders: 1,
                                sourceMap: true
                            },
                        }, {
                            loader: "resolve-url-loader"
                        }, {
                            loader: "sass-loader",
                            options: { sourceMap: true },
                        },
                    ],
                }),
            },
        ],
    },
    externals: {
        "fs": "node",
        "react-native-sqlite-storage": "node",
    },
    devtool: "source-map",
    devServer: {
        port: 4001,
        historyApiFallback: true
    },
    plugins: [
        extractCSS,
    ],
};
