import * as React from "react";
import { action } from "mobx";
import { observer } from "mobx-react";
import { History } from "history";
import { field, Field, hasFields } from "hyrest-mobx";
import { Form, Input, Button } from "antd";
import { Icon } from "react-fa";
import { User } from "finny-common";
import { external, inject } from "tsdi";
import { addRoute } from "../routing";
import { StoreUsers } from "../stores";
import { formItem } from "../hyrest-antd";
import { FullsizeForm } from "../components";
import { routeDashboard } from "./dashboard";
import { routeLogin } from "./login";

@observer @external @hasFields()
export class PageSignup extends React.Component {
    @inject private history: History;
    @inject private users: StoreUsers;

    @field(User) private user: Field<User>;

    @action.bound private async handleSubmit(e: React.SyntheticEvent<HTMLFormElement>) {
        e.preventDefault();
        if (!await this.users.create(this.user.value)) { return; }
        this.history.push(routeDashboard.path());
    }

    @action.bound private handleLogin() {
        this.history.push(routeLogin.path());
    }

    public render () {
        const { email, password, name } = this.user.nested;
        return (
            <FullsizeForm title="Signup">
                <Form onSubmit={this.handleSubmit}>
                    <Form.Item {...formItem(email)}>
                        <Input
                            addonBefore={<Icon name="envelope" />}
                            placeholder="Email"
                            {...email.reactInput}
                        />
                    </Form.Item>
                    <Form.Item {...formItem(name)}>
                        <Input
                            addonBefore={<Icon name="user" />}
                            placeholder="Username"
                            {...name.reactInput}
                        />
                    </Form.Item>
                    <Form.Item {...formItem(password)}>
                        <Input
                            addonBefore={<Icon name="lock" />}
                            placeholder="Password"
                            type="password"
                            {...password.reactInput}
                        />
                    </Form.Item>
                    <Form.Item>
                        <Button disabled={!this.user.valid} type="primary" htmlType="submit">Signup</Button>
                    </Form.Item>
                    <p>Already have an account? Login <a onClick={this.handleLogin}>here</a>.</p>
                </Form>
            </FullsizeForm>
        );
    }
}

export const routeSignup = {
    path: () => "signup",
    pattern: "/signup",
    component: PageSignup,
};

addRoute(routeSignup);
