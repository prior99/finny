import * as React from "react";
import { Card, Row, Col } from "antd";
import { addRoute, RouteProps } from "../routing";
import { ChartTagging, ChartMonthlyProfit, ChartMonthlyDiverging, ChartMonthlyBalance } from "../components";

export class PageSEPAAccountAnalysis extends React.Component<RouteProps<{ id: string }>> {
    private get id() { return this.props.match.params.id; }

    public render () {
        return (
            <div>
                <Row>
                    <Col>
                        <Card bordered={false} title="Monthly profit">
                            <ChartMonthlyProfit sepaAccountId={this.id} />
                        </Card>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <Card bordered={false} title="Monthly Income and Expenses">
                            <ChartMonthlyDiverging sepaAccountId={this.id} />
                        </Card>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <Card bordered={false} title="Monthly Balance">
                            <ChartMonthlyBalance sepaAccountId={this.id} />
                        </Card>
                    </Col>
                </Row>
                <Row>
                    <Col span={12}>
                        <Card bordered={false} title="Tagged Expenses">
                            <ChartTagging mode="expenses" sepaAccountId={this.id} />
                        </Card>
                    </Col>
                    <Col span={12}>
                        <Card bordered={false} title="Tagged Income">
                            <ChartTagging mode="income" sepaAccountId={this.id} />
                        </Card>
                    </Col>
                </Row>
            </div>
        );
    }
}

export const routeSEPAAccountAnalysis = {
    path: (id: string) => `/sepa-account-analysis/${id}`,
    pattern: "/sepa-account-analysis/:id",
    component: PageSEPAAccountAnalysis,
};

addRoute(routeSEPAAccountAnalysis);
