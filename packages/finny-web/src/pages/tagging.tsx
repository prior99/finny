import * as React from "react";
import { observer } from "mobx-react";
import { observable, action } from "mobx";
import { external } from "tsdi";
import { Icon } from "react-fa";
import { Card, Button } from "antd";
import { addRoute } from "../routing";
import { TagModal, ClassifierModal, TableClassifiers, TableTags, TableTransactions } from "../components";

@observer @external
export class PageTagging extends React.Component {
    @observable private showTagForm = false;
    @observable private showClassifierForm = false;

    @action.bound private handleExpandTagForm() {
        this.showTagForm = !this.showTagForm;
    }

    @action.bound private handleExpandClassifierForm() {
        this.showClassifierForm = !this.showClassifierForm;
    }

    public render () {
        return (
            <div>
                <TagModal visible={this.showTagForm} onClose={this.handleExpandTagForm} />
                <Card bordered={false} title="Tags">
                    <TableTags />
                    <div style={{ marginTop: 10 }}>
                        <Button
                            style={{ marginLeft: 10 }}
                            size="large"
                            type="primary"
                            onClick={this.handleExpandTagForm}
                        >
                            <Icon name="tag" /> Add Tag
                        </Button>
                    </div>
                </Card>
                <Card bordered={false} title="Unclassified Transactions">
                    <TableTransactions unclassified />
                </Card>
            </div>
        );
    }
}

export const routeTagging = {
    path: () => "/tagging",
    pattern: "/tagging",
    component: PageTagging,
};

addRoute(routeTagging);
