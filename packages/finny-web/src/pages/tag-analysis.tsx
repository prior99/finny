import * as React from "react";
import { observer } from "mobx-react";
import { observable, action } from "mobx";
import { external } from "tsdi";
import { Icon } from "react-fa";
import { Card, Button, Row, Col } from "antd";
import { addRoute } from "../routing";
import {
    TagModal,
    ClassifierModal,
    TableClassifiers,
    TableTags,
    TableTransactions,
    ChartTagging,
} from "../components";

@observer @external
export class PageTagAnalysis extends React.Component {
    public render () {
        return (
            <Row>
                <Col span={12}>
                    <Card bordered={false} title="Expenses">
                        <ChartTagging mode="expenses" ignoreInternal />
                    </Card>
                </Col>
                <Col span={12}>
                    <Card bordered={false} title="Income">
                        <ChartTagging mode="income" ignoreInternal />
                    </Card>
                </Col>
            </Row>
        );
    }
}

export const routeTagAnalysis = {
    path: () => "/tag-analysis",
    pattern: "/tag-analysis",
    component: PageTagAnalysis,
};

addRoute(routeTagAnalysis);
