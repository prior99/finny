import * as React from "react";
import { addRoute } from "../routing";

export class PageDashboard extends React.Component {
    public render () {
        return (
            <div>
            </div>
        );
    }
}

export const routeDashboard = {
    path: () => "dashboard",
    pattern: "/dashboard",
    component: PageDashboard,
    navbar: true,
    icon: "dashboard",
    title: "Dashboard",
};

addRoute(routeDashboard);
