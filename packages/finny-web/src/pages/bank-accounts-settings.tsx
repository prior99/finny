import * as React from "react";
import { History } from "history";
import { observer } from "mobx-react";
import { observable, action } from "mobx";
import { external, inject } from "tsdi";
import { Icon } from "react-fa";
import { Card, Button } from "antd";
import { addRoute } from "../routing";
import { routeTagging } from "./tagging";
import { routeTagAnalysis } from "./tag-analysis";
import { StoreSEPAAccounts, StoreFinTSCredentials } from "../stores";
import { TableFinTSCredentials, TableSEPAAccounts, FinTSCredentialsModal } from "../components";

@observer @external
export class PageBankAccountsSettings extends React.Component {
    @inject private sepaAccounts: StoreSEPAAccounts;
    @inject private finTSCredentials: StoreFinTSCredentials;
    @inject private history: History;

    @observable private showForm = false;

    @action.bound private handleExpandForm() {
        this.showForm = !this.showForm;
    }

    @action.bound private async handleSynchronize() {
        await this.finTSCredentials.importAll();
        await this.sepaAccounts.refreshAll();
    }

    @action.bound private handleTaggingClick() {
        this.history.push(routeTagging.path());
    }

    @action.bound private handleTagAnalysisClick() {
        this.history.push(routeTagAnalysis.path());
    }

    public render () {
        return (
            <div>
                <FinTSCredentialsModal visible={this.showForm} onClose={this.handleExpandForm} />
                <Card bordered={false} title="Credentials">
                    <TableFinTSCredentials />
                </Card>
                <Card bordered={false} title="SEPA Accounts">
                    <TableSEPAAccounts />
                </Card>
                <div>
                    <Button size="large" type="primary" onClick={this.handleSynchronize}>
                        <Icon name="refresh" /> Synchronize All
                    </Button>
                    <Button style={{ marginLeft: 10 }} size="large" onClick={this.handleExpandForm}>
                        <Icon name="plus" /> Add Credentials
                    </Button>
                    <Button style={{ marginLeft: 10 }} size="large" onClick={this.handleTaggingClick}>
                        <Icon name="tags" /> Tagging
                    </Button>
                    <Button style={{ marginLeft: 10 }} size="large" onClick={this.handleTagAnalysisClick}>
                        <Icon name="chart-pie" /> Tag Analysis
                    </Button>
                </div>
            </div>
        );
    }
}

export const routeBankAccountsSettings = {
    path: () => "/sepa-accounts",
    pattern: "/sepa-accounts",
    component: PageBankAccountsSettings,
    navbar: true,
    icon: "credit-card",
    title: "SEPA Accounts",
};

addRoute(routeBankAccountsSettings);
