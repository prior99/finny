import * as React from "react";
import { action } from "mobx";
import { observer } from "mobx-react";
import { History } from "history";
import { field, Field, hasFields } from "hyrest-mobx";
import { Form, Input, Button } from "antd";
import { Icon } from "react-fa";
import { User } from "finny-common";
import { external, inject } from "tsdi";
import { addRoute } from "../routing";
import { StoreLogin } from "../stores";
import { formItem } from "../hyrest-antd";
import { FullsizeForm } from "../components";
import { routeDashboard } from "./dashboard";
import { routeSignup } from "./signup";

@observer @external @hasFields()
export class PageLogin extends React.Component {
    @inject private history: History;
    @inject private login: StoreLogin;

    @field(User) private user: Field<User>;

    @action.bound private async handleSubmit(e: React.SyntheticEvent<HTMLFormElement>) {
        e.preventDefault();
        if (!await this.login.login(this.user.value)) { return; }
        this.history.push(routeDashboard.path());
    }

    @action.bound private handleSignup() {
        this.history.push(routeSignup.path());
    }

    public render () {
        const { email, password } = this.user.nested;
        return (
            <FullsizeForm title="Login">
                <Form onSubmit={this.handleSubmit}>
                    <Form.Item {...formItem(email)}>
                        <Input
                            addonBefore={<Icon name="envelope" />}
                            placeholder="Email"
                            {...email.reactInput}
                        />
                    </Form.Item>
                    <Form.Item {...formItem(password)}>
                        <Input
                            addonBefore={<Icon name="lock" />}
                            placeholder="Password"
                            type="password"
                            {...password.reactInput}
                        />
                    </Form.Item>
                    <Form.Item>
                        <Button disabled={!this.user.valid} type="primary" htmlType="submit">Login</Button>
                    </Form.Item>
                    <p>You don't have an account? Signup <a onClick={this.handleSignup}>here</a>.</p>
                </Form>
            </FullsizeForm>
        );
    }
}

export const routeLogin = {
    path: () => "login",
    pattern: "/login",
    component: PageLogin,
};

addRoute(routeLogin);
