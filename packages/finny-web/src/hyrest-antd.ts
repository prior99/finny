import { Field } from "hyrest-mobx";

interface FormItemProps {
    help?: string;
    validateStatus?: "validating" | "error" | "success";
}

export function formItem(field: Field<any>): FormItemProps {
    if (field.valid) { return { validateStatus: "success" }; }
    if (field.unknown) { return {}; }
    if (field.inProgress) { return { validateStatus: "validating" }; }
    if (field.invalid) {
        return {
            help: field.error,
            validateStatus: "error",
        };
    }
}
