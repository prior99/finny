import * as React from "react";
import { Card } from "antd";
import * as css from "./app-container.scss";

export class FullsizeForm extends React.Component<{ title: string }> {
    public render() {
        const { children, title } = this.props;
        return (
            <section className={css.wrapper}>
                <article className={css.inner}>
                    <Card title={title}>
                        {children}
                    </Card>
                </article>
            </section>
        );
    }
}
