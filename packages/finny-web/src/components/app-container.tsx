import * as React from "react";
import { Layout } from "antd";
import { external, inject } from "tsdi";
import { observer } from "mobx-react";
import { StoreLogin, StoreUsers } from "../stores";
import { TopBar } from "./top-bar";
import * as css from "./app-container.scss";

@external @observer
export class AppContainer extends React.Component {
    @inject private login: StoreLogin;
    @inject private users: StoreUsers;

    public render() {
        const { loggedIn } = this.login;
        if (!loggedIn) { return this.props.children; }
        if (!this.users.ownUser) { return null; }
        return (
            <Layout>
                <TopBar />
                <Layout.Content>
                    <div className={css.container}>
                        {this.props.children}
                    </div>
                </Layout.Content>
            </Layout>
        );
    }
}
