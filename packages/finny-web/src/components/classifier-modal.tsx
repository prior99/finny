import * as React from "react";
import { action, observable, computed } from "mobx";
import { observer } from "mobx-react";
import { pick } from "ramda";
import { field, Field, hasFields } from "hyrest-mobx";
import { Form, Input, Modal, Switch, AutoComplete } from "antd";
import { Icon } from "react-fa";
import { Classifier } from "finny-common";
import { external, inject } from "tsdi";
import { StoreClassifiers, StoreTags } from "../stores";
import { formItem } from "../hyrest-antd";
import * as css from "./classifier-modal.scss";

interface ClassifierModalProps {
    tagId?: string;
    visible: boolean;
    onClose: () => void;
}

@observer @external @hasFields()
export class ClassifierModal extends React.Component<ClassifierModalProps> {
    @inject private classifiers: StoreClassifiers;
    @inject private tags: StoreTags;
    @observable private loading = false;
    @observable private activeRules: (keyof Classifier)[] = [];
    @observable private selectedTag: string;

    @field(Classifier) private classifier: Field<Classifier>;

    private toggle(identifier: keyof Classifier) {
        return action(() => {
            if (this.activeRules.includes(identifier)) {
                this.activeRules = this.activeRules.filter(current => current !== identifier);
            } else {
                this.activeRules.push(identifier);
            }
        });
    }

    private isActive(identifier: keyof Classifier) {
        return this.activeRules.includes(identifier);
    }

    private switchProps(identifier: keyof Classifier) {
        return {
            checked: this.isActive(identifier),
            onChange: this.toggle(identifier),
        };
    }

    @action.bound private handleTagChange(value: string) {
        this.selectedTag = value;
    }

    @action.bound private async handleSubmit() {
        this.loading = true;
        const { tagId } = this.props;
        try {
            await this.classifiers.create({
                ...pick(this.activeRules, this.classifier.value),
                tag: { id: tagId ? tagId : this.selectedTag },
            } as Classifier);
            this.props.onClose();
        } catch (error) {} // tslint:disable-line
        this.loading = false;
    }

    @computed private get invalid() {
        return !this.classifier.valid ||
            this.activeRules.length === 0 ||
            (!this.selectedTag && !this.props.tagId);
    }

    public render () {
        const { iban, nameRegexp, textRegexp, referenceRegexp, thresholdMin, thresholdMax } = this.classifier.nested;
        return (
            <Modal
                visible={this.props.visible}
                title="Add Classifier"
                onOk={this.handleSubmit}
                onCancel={this.props.onClose}
                confirmLoading={this.loading}
                okButtonProps={{ disabled: this.invalid }}
            >
                <Form onSubmit={this.handleSubmit}>
                    {
                        !this.props.tagId && (
                            <Form.Item>
                                <AutoComplete
                                    dataSource={this.tags.autoCompleteOptions}
                                    placeholder="Tag"
                                    onSelect={this.handleTagChange}
                                    value={this.selectedTag}
                                />
                            </Form.Item>
                        )
                    }
                    <Form.Item {...formItem(iban)}>
                        <span className={css.inputWrapper}>
                            <Switch {...this.switchProps("iban")} />
                            <Input
                                disabled={!this.isActive("iban")}
                                prefix={<Icon name="address-card" />}
                                placeholder="IBAN"
                                {...iban.reactInput}
                            />
                        </span>
                    </Form.Item>
                    <Form.Item {...formItem(nameRegexp)}>
                        <span className={css.inputWrapper}>
                            <Switch {...this.switchProps("nameRegexp")} />
                            <Input
                                disabled={!this.isActive("nameRegexp")}
                                prefix={<Icon name="user" />}
                                placeholder="Name"
                                {...nameRegexp.reactInput}
                            />
                        </span>
                    </Form.Item>
                    <Form.Item {...formItem(textRegexp)}>
                        <span className={css.inputWrapper}>
                            <Switch {...this.switchProps("textRegexp")} />
                            <Input
                                disabled={!this.isActive("textRegexp")}
                                prefix={<Icon name="font" />}
                                placeholder="Text"
                                {...textRegexp.reactInput}
                            />
                        </span>
                    </Form.Item>
                    <Form.Item {...formItem(referenceRegexp)}>
                        <span className={css.inputWrapper}>
                            <Switch {...this.switchProps("referenceRegexp")} />
                            <Input
                                disabled={!this.isActive("referenceRegexp")}
                                prefix={<Icon name="envelope-open-text" />}
                                placeholder="Reference"
                                {...referenceRegexp.reactInput}
                            />
                        </span>
                    </Form.Item>
                    <Form.Item {...formItem(thresholdMax)}>
                        <span className={css.inputWrapper}>
                            <Switch {...this.switchProps("thresholdMax")} />
                            <Input
                                disabled={!this.isActive("thresholdMax")}
                                prefix={<Icon name="greater-than-equal" />}
                                type="number"
                                placeholder="Minimum Amount"
                                {...thresholdMax.reactInput}
                            />
                        </span>
                    </Form.Item>
                    <Form.Item {...formItem(thresholdMin)}>
                        <span className={css.inputWrapper}>
                            <Switch {...this.switchProps("thresholdMin")} />
                            <Input
                                disabled={!this.isActive("thresholdMin")}
                                prefix={<Icon name="less-than" />}
                                placeholder="Maximum Amount"
                                {...thresholdMin.reactInput}
                            />
                        </span>
                    </Form.Item>
                </Form>
            </Modal>
        );
    }
}
