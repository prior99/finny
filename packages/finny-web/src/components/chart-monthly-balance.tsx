import * as React from "react";
import { format } from "date-fns";
import { ResponsiveLine } from "@nivo/line";
import { external, inject } from "tsdi";
import { observer } from "mobx-react";
import { computed } from "mobx";
import { StoreStatements } from "../stores";

@observer @external
export class ChartMonthlyBalance extends React.Component<{ sepaAccountId: string }> {
    @inject private statements: StoreStatements;

    @computed private get data() {
        const analysis = this.statements.getMonthlyAnalysis(this.props.sepaAccountId);
        return [
            {
                id: "min balance",
                data: analysis.map(({ month, minBalance }) => ({ x: month, y: minBalance })),
            }, {
                id: "max balance",
                data: analysis.map(({ month, maxBalance }) => ({ x: month, y: maxBalance })),
            },
        ];
    }

    public render() {
        return (
            <div style={{ height: 400 }}>
                <ResponsiveLine
                    data={this.data}
                    curve="monotoneX"
                    colors={["#e25c3b", "#61cdbb"]}
                    margin={{
                        top: 50,
                        right: 110,
                        bottom: 50,
                        left: 60,
                    }}
                    xScale={{ type: "time" }}
                    yScale={{
                        type: "linear",
                        min: "auto",
                        max: "auto",
                    }}
                    axisBottom={{
                        legend: "month",
                        legendOffset: 36,
                        legendPosition: "center",
                        format: date => format(date, "YYYY-MM"),
                    }}
                    axisLeft={{
                        legend: "balance",
                        legendOffset: -40,
                        legendPosition: "center",
                    }}
                    tooltipFormat={value => `${value.toFixed(2)} EUR`}
                />
            </div>
        );
    }
}
