import * as React from "react";
import { format } from "date-fns";
import { ResponsiveBar } from "@nivo/bar";
import { external, inject } from "tsdi";
import { observer } from "mobx-react";
import { computed, observable, action } from "mobx";
import { Modal, Row, Col, Card } from "antd";
import { StoreStatements } from "../stores";
import { ChartTagging } from "./chart-tagging";

const AnyResponsiveBar = ResponsiveBar as any;

@observer @external
export class ChartMonthlyDiverging extends React.Component<{ sepaAccountId: string }> {
    @inject private statements: StoreStatements;

    @observable private activeMonth: Date;

    @computed private get data() {
        return this.statements.getMonthlyAnalysis(this.props.sepaAccountId)
            .map(({ month, sumExpenses, sumIncome }) => ({
                expenses: -sumExpenses,
                income: sumIncome,
                month: format(month, "YYYY-MM"),
                date: month,
            }));
    }

    @action.bound private handleResetActiveMonth() {
        this.activeMonth = undefined;
    }

    @action.bound private handleClick({ data }: { data: { date: Date } }) {
        this.activeMonth = data.date;
    }

    private renderActiveMonthModal() {
        if (!this.activeMonth) { return null; }
        return (
            <Modal visible={true} width="90vw" onOk={this.handleResetActiveMonth}>
                <Row>
                    <Col span={12}>
                        <Card bordered={false} title="Expenses">
                            <ChartTagging
                                mode="expenses"
                                sepaAccountId={this.props.sepaAccountId}
                                month={this.activeMonth}
                            />
                        </Card>
                    </Col>
                    <Col span={12}>
                        <Card bordered={false} title="Income">
                            <ChartTagging
                                mode="income"
                                sepaAccountId={this.props.sepaAccountId}
                                month={this.activeMonth}
                            />
                        </Card>
                    </Col>
                </Row>
            </Modal>
        );
    }

    public render() {
        return (
            <div style={{ height: 400 }}>
                {this.renderActiveMonthModal()}
                <AnyResponsiveBar
                    indexBy="month"
                    enableGridX
                    enableGridY={false}
                    data={this.data}
                    labelSkipWidth={50}
                    onClick={this.handleClick}
                    onCancel={this.handleClick}
                    margin={{
                        top: 50,
                        right: 110,
                        bottom: 60,
                        left: 60,
                    }}
                    keys={["income", "expenses"]}
                    axisBottom={{
                        tickSize: 0,
                        tickPadding: 12,
                        tickRotation: -90,
                    }}
                    colors={["#61cdbb", "#e25c3b"]}
                />
            </div>
        );
    }
}
