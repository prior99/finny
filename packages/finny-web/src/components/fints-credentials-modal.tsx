import * as React from "react";
import { action, observable } from "mobx";
import { observer } from "mobx-react";
import { field, Field, hasFields } from "hyrest-mobx";
import { Form, Input, Modal } from "antd";
import { Icon } from "react-fa";
import { FinTSCredentials } from "finny-common";
import { external, inject } from "tsdi";
import { StoreFinTSCredentials } from "../stores";
import { formItem } from "../hyrest-antd";

@observer @external @hasFields()
export class FinTSCredentialsModal extends React.Component<{ visible: boolean, onClose: () => void }> {
    @inject private fintsCredentials: StoreFinTSCredentials;
    @observable private loading = false;

    @field(FinTSCredentials) private credentials: Field<FinTSCredentials>;

    @action.bound private async handleSubmit() {
        this.loading = true;
        try {
            await this.fintsCredentials.create(this.credentials.value);
            this.props.onClose();
        } catch (error) {} // tslint:disable-line
        this.loading = false;
    }

    public render () {
        const { name, blz, url, pin } = this.credentials.nested;
        return (
            <Modal
                visible={this.props.visible}
                title="Add Credentials"
                onOk={this.handleSubmit}
                onCancel={this.props.onClose}
                confirmLoading={this.loading}
                okButtonProps={{ disabled: !this.credentials.valid }}
            >
                <Form onSubmit={this.handleSubmit}>
                    <Form.Item {...formItem(blz)}>
                        <Input
                            prefix={<Icon name="university" />}
                            placeholder="BLZ"
                            {...blz.reactInput}
                        />
                    </Form.Item>
                    <Form.Item {...formItem(url)}>
                        <Input
                            prefix={<Icon name="link" />}
                            placeholder="URL"
                            {...url.reactInput}
                        />
                    </Form.Item>
                    <Form.Item {...formItem(name)}>
                        <Input
                            prefix={<Icon name="user" />}
                            placeholder="Username"
                            {...name.reactInput}
                        />
                    </Form.Item>
                    <Form.Item {...formItem(pin)}>
                        <Input
                            prefix={<Icon name="key" />}
                            type="password"
                            placeholder="PIN"
                            {...pin.reactInput}
                        />
                    </Form.Item>
                </Form>
            </Modal>
        );
    }
}
