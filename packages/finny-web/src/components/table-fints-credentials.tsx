import * as React from "react";
import { observer } from "mobx-react";
import { action } from "mobx";
import { FinTSCredentials } from "finny-common";
import { Icon } from "react-fa";
import { external, inject } from "tsdi";
import { Table, Button } from "antd";
import { StoreFinTSCredentials, StoreSEPAAccounts } from "../stores";

@observer @external
class Actions extends React.Component<{ id: string }> {
    @inject private sepaAccounts: StoreSEPAAccounts;

    @action.bound private async handleSynchronize() {
        await this.sepaAccounts.finTSImport(this.props.id);
    }

    public render() {
        return (
            <Button.Group>
                <Button onClick={this.handleSynchronize}>
                    <Icon spin={this.sepaAccounts.isImporting(this.props.id)} name="refresh" />
                </Button>
            </Button.Group>
        );
    }
}

@observer @external
export class TableFinTSCredentials extends React.Component {
    @inject private finTSCredentials: StoreFinTSCredentials;

    public render() {
        return (
            <Table
                pagination={false}
                dataSource={this.finTSCredentials.all}
            >
                <Table.Column
                    title="BLZ"
                    dataIndex="blz"
                    key="blz"
                />
                <Table.Column
                    title="Name"
                    dataIndex="name"
                    key="name"
                />
                <Table.Column
                    title="URL"
                    dataIndex="url"
                    key="url"
                />
                <Table.Column
                    key="actions"
                    render={(text: string, { id }: FinTSCredentials) => <Actions id={id} />}
                />
            </Table>
        );
    }
}
