import * as React from "react";
import { comparator } from "ramda";
import { observer } from "mobx-react";
import { format } from "date-fns";
import { computed, action, observable } from "mobx";
import { external, inject, initialize } from "tsdi";
import { Table, Pagination } from "antd";
import { StoreTransactions } from "../stores";
import { Transaction } from "finny-common";

@observer @external
export class TableTransactions extends React.Component<{ unclassified?: boolean, hideInternal?: boolean }> {
    @inject protected transactions: StoreTransactions;

    @action.bound private async fetch(page: number, limit: number) {
        return await this.transactions.search(page, limit, this.props.unclassified, this.props.hideInternal);
    }

    @initialize protected async initialize() {
        await this.fetch(0, 50);
    }

    @computed private get data() {
        if (!this.currentResult) { return []; }
        return this.currentResult.results.map(transaction => {
            return {
                ...transaction,
                reference: transaction.reference.text || transaction.reference.raw,
                valueDate: format(transaction.valueDate, "YYYY-MM-DD"),
                key: transaction.id,
            };
        });
    }

    @computed private get currentResult() { return this.transactions.currentResult; }

    @computed private get pagination() {
        const { page = 0, limit = 0, total = 0 } = this.currentResult ? this.currentResult : {};
        return {
            current: page,
            pageSize: limit,
            total,
        };
    }

    @action.bound private async handleTabChange(pagination: { pageSize: number, current: number }) {
        return await this.fetch(pagination.current, pagination.pageSize);
    }

    public render() {
        return (
            <Table
                size="middle"
                bordered={false}
                onChange={this.handleTabChange}
                pagination={this.pagination}
                dataSource={this.data}
                loading={this.transactions.loading}
            >
                <Table.Column
                    title="Text"
                    dataIndex="text"
                    key="text"
                />
                <Table.Column
                    title="IBAN"
                    dataIndex="iban"
                    key="iban"
                />
                <Table.Column
                    title="Name"
                    dataIndex="name"
                    key="name"
                    sorter={comparator<Transaction>((a, b) => a.name < b.name)}
                />
                <Table.Column
                    title="Date"
                    dataIndex="valueDate"
                    key="valueDate"
                    sorter={comparator<Transaction>((a, b) => a.valueDate < b.valueDate)}
                />
                <Table.Column
                    title="Amount"
                    dataIndex="amount"
                    key="amount"
                    sorter={comparator<Transaction>((a, b) => a.amount < b.amount)}
                />
                <Table.Column
                    title="Reference"
                    dataIndex="reference"
                    key="reference"
                />
            </Table>
        );
    }
}
