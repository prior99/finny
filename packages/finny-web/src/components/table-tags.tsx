import * as React from "react";
import { comparator } from "ramda";
import { observer } from "mobx-react";
import { computed, action, observable } from "mobx";
import { Icon } from "react-fa";
import { external, inject } from "tsdi";
import { Tag } from "finny-common";
import { Button, Table, Popconfirm } from "antd";
import { StoreTags, StoreClassifiers } from "../stores";
import { TableClassifiers } from "./table-classifiers";
import { ClassifierModal } from "./classifier-modal";
import { TagModal } from "./tag-modal";

@observer @external
class Actions extends React.Component<{ id: string }> {
    @inject private tags: StoreTags;

    @observable private addingClassifier = false;
    @observable private addingChild = false;

    @action.bound private async handleDelete() {
        await this.tags.delete(this.props.id);
    }

    @action.bound private async handleAddChild() {
        this.addingChild = !this.addingChild;
    }

    @action.bound private async handleClassifierModalToggle() {
        this.addingClassifier = !this.addingClassifier;
    }

    public render() {
        return (
            <>
                <TagModal
                    parentId={this.props.id}
                    visible={this.addingChild}
                    onClose={this.handleAddChild}
                />
                <ClassifierModal
                    tagId={this.props.id}
                    visible={this.addingClassifier}
                    onClose={this.handleClassifierModalToggle}
                />
                <Button.Group>
                    <Popconfirm
                        placement="top"
                        title="Are you sure?"
                        onConfirm={this.handleDelete}
                        okText="Yes"
                        cancelText="No"
                    >
                        <Button shape="circle">
                            <Icon name="trash" />
                        </Button>
                    </Popconfirm>
                    <Button onClick={this.handleAddChild} shape="circle">
                        <Icon name="plus" />
                    </Button>
                    <Button onClick={this.handleClassifierModalToggle} shape="circle">
                        <Icon name="filter" />
                    </Button>
                </Button.Group>
            </>
        );
    }
}

interface TagTree extends Tag {
    children: TagTree[];
    key: string;
}

@observer @external
export class TableTags extends React.Component {
    @inject private tags: StoreTags;
    @inject private classifiers: StoreClassifiers;

    @computed private get data(): TagTree[] {
        const  { rootTags } = this.tags;
        const constructTree = (tag: Tag): TagTree => {
            const children = this.tags.all
                .filter(current => current.parent && current.parent.id === tag.id)
                .map(constructTree);
            return {
                ...tag,
                classificationsCount: tag.classificationsCount,
                children: children.length > 0 ? children : undefined,
                key: tag.id,
            };
        };
        return rootTags.map(constructTree);
    }

    private renderTagTable(row: Tag) {
        return <TableClassifiers tagId={row.id} />;
    }

    public render() {
        return (
            <Table
                size="middle"
                bordered={false}
                dataSource={this.data}
                expandedRowRender={this.renderTagTable}
                loading={this.tags.loading || this.classifiers.loading}
            >
                <Table.Column
                    title="Name"
                    dataIndex="name"
                    key="name"
                    sorter={comparator<Tag>((a, b) => a.name < b.name)}
                />
                <Table.Column
                    title="Transactions"
                    dataIndex="classificationsCount"
                    key="classificationsCount"
                    sorter={comparator<Tag>((a, b) => a.classificationsCount < b.classificationsCount)}
                />
                <Table.Column
                    key="actions"
                    align="right"
                    render={(text: string, { id }: Tag) => <Actions id={id} />}
                />
            </Table>
        );
    }
}
