import * as React from "react";
import { History } from "history";
import { observer } from "mobx-react";
import { action } from "mobx";
import { external, inject } from "tsdi";
import { Icon } from "react-fa";
import { SEPAAccount } from "finny-common";
import { Button, Table } from "antd";
import { routeSEPAAccountAnalysis } from "../pages";
import { StoreSEPAAccounts, StoreStatements } from "../stores";

@observer @external
class Actions extends React.Component<{ id: string }> {
    @inject private statements: StoreStatements;
    @inject private history: History;

    @action.bound private async handleSynchronize() {
        await this.statements.refreshStatements(this.props.id);
    }

    @action.bound private async handleAnalysis() {
        this.history.push(routeSEPAAccountAnalysis.path(this.props.id));
    }

    public render() {
        return (
            <Button.Group>
                <Button type="primary" onClick={this.handleAnalysis}>
                    <Icon name="calculator" />
                </Button>
                <Button onClick={this.handleSynchronize}>
                    <Icon spin={this.statements.isRefreshing(this.props.id)} name="refresh" />
                </Button>
            </Button.Group>
        );
    }
}

@observer @external
export class TableSEPAAccounts extends React.Component {
    @inject private sepaAccounts: StoreSEPAAccounts;

    public render() {
        return (
            <Table
                pagination={false}
                dataSource={this.sepaAccounts.all}
            >
                <Table.Column
                    title="IBAN"
                    dataIndex="iban"
                    key="iban"
                />
                <Table.Column
                    title="BIC"
                    dataIndex="bic"
                    key="bic"
                />
                <Table.Column
                    key="actions"
                    render={(text: string, { id }: SEPAAccount) => <Actions id={id}/>}
                />
            </Table>
        );
    }
}
