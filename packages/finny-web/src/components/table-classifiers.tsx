import * as React from "react";
import { external, inject } from "tsdi";
import { observer } from "mobx-react";
import { Icon } from "react-fa";
import { computed, action } from "mobx";
import { Button, Table, Popconfirm } from "antd";
import { Classifier } from "finny-common";
import { StoreClassifiers, StoreTags } from "../stores";

@observer @external
class Actions extends React.Component<{ id: string }> {
    @inject private classifiers: StoreClassifiers;

    @action.bound private async handleDelete() {
        await this.classifiers.delete(this.props.id);
    }

    public render() {
        return (
            <>
                <Popconfirm
                    placement="top"
                    title="Are you sure?"
                    onConfirm={this.handleDelete}
                    okText="Yes"
                    cancelText="No"
                >
                    <Button shape="circle" type="danger">
                        <Icon name="trash" />
                    </Button>
                </Popconfirm>
            </>
        );
    }
}

@external @observer
export class TableClassifiers extends React.Component<{ tagId?: string }> {
    @inject private classifiers: StoreClassifiers;
    @inject private tags: StoreTags;

    @computed private get data() {
        const { tagId } = this.props;
        return this.classifiers.all
            .filter(row => !tagId || row.tag.id === tagId)
            .map(classifier => {
                const tag = this.tags.byId(classifier.tag.id);
                return {
                    ...classifier,
                    classificationsCount: classifier.classificationsCount,
                    tag: tag ? tag.name : "",
                    key: classifier.id,
                };
            });
    }

    public render() {
        return (
            <Table
                size="small"
                bordered={false}
                pagination={false}
                dataSource={this.data}
                style={{ background: "white" }}
            >
                {
                    !this.props.tagId && (
                        <Table.Column
                            title="Tag"
                            dataIndex="tag"
                            key="tag"
                        />
                    )
                }
                <Table.Column
                    title="IBAN"
                    dataIndex="iban"
                    key="iban"
                />
                <Table.Column
                    title="Name"
                    dataIndex="nameRegexp"
                    key="nameRegexp"
                />
                <Table.Column
                    title="Text"
                    dataIndex="textRegexp"
                    key="textRegexp"
                />
                <Table.Column
                    title="Reference"
                    dataIndex="referenceRegexp"
                    key="referenceRegexp"
                />
                <Table.Column
                    title="Min"
                    dataIndex="thresholdMin"
                    key="thresholdMin"
                />
                <Table.Column
                    title="Max"
                    dataIndex="thresholdMax"
                    key="thresholdMax"
                />
                <Table.Column
                    title="Transactions"
                    dataIndex="classificationsCount"
                    key="classificationsCount"
                />
                <Table.Column
                    key="actions"
                    align="right"
                    render={(text: string, { id }: Classifier) => <Actions id={id} />}
                />
            </Table>
        );
    }
}
