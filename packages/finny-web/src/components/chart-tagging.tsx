import * as React from "react";
import { format } from "date-fns";
import { Sunburst } from "react-vis";
import { external, inject, initialize } from "tsdi";
import { observer } from "mobx-react";
import { action, computed, observable } from "mobx";
import { TagAnalysis } from "finny-common";
import { StoreTags } from "../stores";
import { stringToColor } from "../string-to-color";
import * as css from "./chart-tagging.scss";

interface ChartTaggingProps {
    mode: "income" | "expenses";
    sepaAccountId?: string;
    month?: Date;
    ignoreInternal?: boolean;
}

@observer @external
export class ChartTagging extends React.Component<ChartTaggingProps> {
    @inject private tags: StoreTags;

    @observable private analysis: TagAnalysis[] = [];
    @observable private selectedTagId: string;
    @observable private locked = false;

    @observable private size = 400;

    @initialize private async initialize() {
        const resize = () => {
            console.log("resize", window.innerHeight - 100, window.innerWidth / 2 - 100)
            this.size = Math.min(window.innerHeight - 100, window.innerWidth / 2 - 100);
        };
        resize();
        window.addEventListener("resize", resize);
        const { sepaAccountId, month, ignoreInternal } = this.props;
        this.analysis = await this.tags.analysis(sepaAccountId, ignoreInternal, month);
    }

    private get totalAmount() {
        return this.analysis.reduce((sum, current) => {
            const value = this.props.mode === "expenses" ? current.totalExpenses : current.totalIncome;
            return sum + value;
        }, 0);
    }

    private findTagAnalysis(tagId: string, array?: TagAnalysis[]) {
        for (let analysis of array || this.analysis) {
            if (analysis.tag && analysis.tag.id === tagId) { return analysis; }
            const childResult = this.findTagAnalysis(tagId, analysis.children);
            if (childResult) { return childResult; }
        }
    }

    private mapEntry(entry: TagAnalysis) {
        const tag = entry.tag ? this.tags.byId(entry.tag.id) : undefined;
        const size = this.props.mode === "expenses" ? entry.exclusiveExpenses : entry.exclusiveIncome;
        const isInSelection = !this.selectedTagId || this.tags.isParentOf(this.selectedTagId, tag && tag.id);
        return {
            children: entry.children.map(child => this.mapEntry(child)),
            size,
            color: tag ? this.tags.color(tag.id) : "#fff",
            title: tag ? tag.name : "Not tagged",
            id: tag ? tag.id : undefined,
            style: {
                fillOpacity: isInSelection ? 1 : 0.2,
                strokeOpacity: isInSelection ? 1 : 0,
            },
        };
    }

    @computed private get selectedTag() { return this.tags.byId(this.selectedTagId); }

    @computed private get selectedTagAnalysis() {
        return this.findTagAnalysis(this.selectedTagId);
    }

    @computed private get data() {
        return {
            name: this.props.mode,
            children: this.analysis.map(entry => this.mapEntry(entry)),
        };
    }

    private get valueKey() {
        const { mode } = this.props;
        if (mode === "expenses") { return "exclusiveExpenses"; }
        return "exclusiveIncome";
    }

    @action.bound private handleValueMouseOver({ id }) {
        if (this.locked) { return; }
        this.selectedTagId = id;
    }

    @action.bound private handleValueMouseOut() {
        if (this.locked) { return; }
        this.selectedTagId = undefined;
    }

    @action.bound private handleValueClick() {
        this.locked = !this.locked;
    }

    private renderValue() {
        if (!this.selectedTagId) { return null; }
        const value = this.props.mode === "expenses" ?
            this.selectedTagAnalysis.totalExpenses :
            this.selectedTagAnalysis.totalIncome;
        const percentage = Math.round((value / this.totalAmount) * 100);
        return (
            <div className={css.value}>
                <p style={{ color: this.tags.color(this.selectedTagId)}}>
                    {this.selectedTag.name}
                </p>
                <p>
                    {Math.round(value)} EUR
                </p>
                <p>
                    {percentage}%
                </p>
            </div>
        );
    }

    public render() {
        if (this.tags.loading) { return null; }
        return (
            <div className={css.wrapper} style={{ width: this.size, height: this.size }}>
                <Sunburst
                    animation
                    data={this.data}
                    hideRootNode
                    colorType="literal"
                    height={this.size}
                    width={this.size}
                    style={{ stroke: "#ddd" }}
                    onValueMouseOver={this.handleValueMouseOver}
                    onValueMouseOut={this.handleValueMouseOut}
                    onValueClick={this.handleValueClick}
                />
                {this.renderValue()}
            </div>
        );
    }
}
