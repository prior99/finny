import * as React from "react";
import { action, observable } from "mobx";
import { observer } from "mobx-react";
import { field, Field, hasFields } from "hyrest-mobx";
import { Form, Input, Modal, AutoComplete } from "antd";
import { Icon } from "react-fa";
import { Tag } from "finny-common";
import { external, inject } from "tsdi";
import { StoreTags } from "../stores";
import { formItem } from "../hyrest-antd";

export interface TagModalProps {
    visible: boolean;
    onClose: () => void;
    parentId?: string;
}

@observer @external @hasFields()
export class TagModal extends React.Component<TagModalProps> {
    @inject private tags: StoreTags;
    @observable private loading = false;
    @observable private selectedParentTag: string;

    @field(Tag) private tag: Field<Tag>;

    @action.bound private handleParentChange(value: string) {
        this.selectedParentTag = value;
    }

    @action.bound private async handleSubmit(e: React.SyntheticEvent<HTMLFormElement>) {
        e.preventDefault();
        this.loading = true;
        const { parentId } = this.props;
        const parent = parentId ? { id: parentId } :
            this.selectedParentTag ? { id: this.selectedParentTag } : undefined;
        try {
            await this.tags.create({ ...this.tag.value, parent } as Tag);
            this.props.onClose();
        } catch (error) {} // tslint:disable-line
        this.loading = false;
    }

    public render () {
        const { name } = this.tag.nested;
        return (
            <Modal
                visible={this.props.visible}
                title="Add Tag"
                onOk={this.handleSubmit}
                onCancel={this.props.onClose}
                confirmLoading={this.loading}
                okButtonProps={{ disabled: !this.tag.valid }}
            >
                <Form onSubmit={this.handleSubmit}>
                    <Form.Item {...formItem(name)}>
                        <Input
                            prefix={<Icon name="tag" />}
                            placeholder="Name"
                            {...name.reactInput}
                        />
                    </Form.Item>
                    {
                        !this.props.parentId && (
                            <Form.Item>
                                <AutoComplete
                                    dataSource={this.tags.autoCompleteOptions}
                                    placeholder="Parent"
                                    onSelect={this.handleParentChange}
                                    value={this.selectedParentTag}
                                />
                            </Form.Item>
                        )

                    }
                </Form>
            </Modal>
        );
    }
}
