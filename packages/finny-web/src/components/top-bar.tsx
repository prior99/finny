import * as React from "react";
import { History } from "history";
import { Layout, Menu } from "antd";
import { Icon } from "react-fa";
import { external, inject } from "tsdi";
import { observer } from "mobx-react";
import { action, computed } from "mobx";
import * as pathToRegexp from "path-to-regexp";
import { StoreLogin } from "../stores";
import {
    routeDashboard,
    routeBankAccountsSettings,
} from "../pages";
import { Route, routes } from "../routing";

@external @observer
export class TopBar extends React.Component {
    @inject private login: StoreLogin;
    @inject private history: History;

    @action.bound private handleLogout() {
        this.login.logout();
    }

    @computed private get selectedKeys() {
        return routes
            .filter(route => pathToRegexp(route.pattern).test(location.pathname))
            .map(route => route.path());
    }

    private routeMenuItem(route: Route) {
        const { icon, title, path } = route;
        const handleClick = () => this.history.push(path());
        return (
            <Menu.Item key={path()} onClick={handleClick}>
                <Icon name={icon} /> {title}
            </Menu.Item>
        );
    }

    public render() {
        return (
            <Layout.Header style={{ padding: 0 }}>
                <Menu
                    mode="horizontal"
                    theme="dark"
                    style={{ lineHeight: "64px" }}
                    defaultSelectedKeys={this.selectedKeys}
                >
                    {this.routeMenuItem(routeDashboard)}
                    {this.routeMenuItem(routeBankAccountsSettings)}
                    <Menu.Item onClick={this.handleLogout}>
                        <Icon style={{ marginRight: 10 }} name="sign-out" />
                        Logout
                    </Menu.Item>
                </Menu>
            </Layout.Header>
        );
    }
}
