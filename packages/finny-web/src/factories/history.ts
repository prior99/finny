import { component, factory } from "tsdi";
import { createBrowserHistory, History } from "history";

@component
export class FactoryHistory {
    @factory({ name: "history" })
    public createHistory(): History {
        return createBrowserHistory();
    }
}
