import { create } from "random-seed";
import * as Color from "color";

export function stringToColor(str: string) {
    const rng = create(str);
    return Color.hsl(rng(360), 80, 40).toString();
}
