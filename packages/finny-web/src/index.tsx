import * as React from "react";
import * as ReactDOM from "react-dom";
import DevTools from "mobx-react-devtools";
import { controllers } from "finny-common";
import { TSDI } from "tsdi";
import "antd/dist/antd.css";
import { configureController } from "hyrest";
import { Route, Switch, Redirect } from "react-router-dom";
import { Router } from "react-router";
import { StoreLogin, StoreErrors } from "./stores";
import { routeLogin, routeDashboard } from "./pages";
import { routes } from "./routing";
import { AppContainer } from "./components";
import "./factories";
import "./global.scss";

// Setup controllers.
declare const baseUrl: string;
configureController(controllers, {
    baseUrl,
    errorHandler: err => {
        console.error(err);
        tsdi.get(StoreErrors).report({
            message: err.answer ? err.answer.message : err.message ? err.message : "Unknown error.",
            fatal: err.statusCode === 401,
        });
        if (err.statusCode === 401) { tsdi.get(StoreLogin).logout(); }
    },
    authorizationProvider: (headers: Headers) => {
        const { loggedIn, authToken } = tsdi.get(StoreLogin);
        if (loggedIn) {
            headers.append("authorization", `Bearer ${authToken}`);
        }
    },
    throwOnError: true,
});

// Setup dependency injection.
const tsdi = new TSDI();
tsdi.enableComponentScanner();

// Root component.
function App() {
    const { loggedIn } = tsdi.get(StoreLogin);
    return (
        <AppContainer>
            <Switch>
                {
                     loggedIn ?
                        <Redirect exact from="/" to={routeDashboard.path()} /> :
                        <Redirect exact from="/" to={routeLogin.path()} />
                }
                {
                    routes.map((route, index) => (
                        <Route
                            exact
                            key={index}
                            path={route.pattern}
                            component={route.component}
                        />
                    ))
                }
            </Switch>
        </AppContainer>
    );
}

// Start React.
ReactDOM.render(
    <div>
        <Router history={tsdi.get("history")}>
            <App />
        </Router>
        <DevTools />
    </div>,
    document.getElementById("root"),
);
