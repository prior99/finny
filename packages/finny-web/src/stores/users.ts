import { Store } from "hyrest-mobx";
import { User, ControllerUsers } from "finny-common";
import { component, inject, initialize } from "tsdi";
import { observable, computed, action } from "mobx";
import { StoreLogin } from "./login";

@component
export class StoreUsers extends Store(ControllerUsers) {
    @inject protected controller: ControllerUsers;
    @inject protected login: StoreLogin;
    @observable private ownId: string;

    @computed public get ownUser() {
        if (!this.ownId) { return; }
        return this.byId(this.ownId);
    }

    @initialize @action.bound protected async initialize() {
        if (!this.login.loggedIn) { return false; }
        try {
            const user = await this.get(this.login.userId);
            this.ownId = user.id;
            return true;
        } catch (err) {
            return false;
        }
    }

    @action.bound public async create(user: User): Promise<User> {
        try {
            const newUser = await super.create(user);
            await this.login.login(newUser);
            return newUser;
        } catch (error) {
            console.error(error);
            return;
        }
    }
}
