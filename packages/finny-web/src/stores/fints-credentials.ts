import { ControllerFinTSCredentials } from "finny-common";
import { Store } from "hyrest-mobx";
import { action } from "mobx";
import { inject, component, initialize } from "tsdi";
import { FinTSCredentials, User } from "finny-common";
import { StoreUsers } from "./users";
import { StoreSEPAAccounts } from "./sepa-accounts";

@component
export class StoreFinTSCredentials extends Store(ControllerFinTSCredentials) {
    @inject protected controller: ControllerFinTSCredentials;
    @inject private users: StoreUsers;
    @inject private sepaAccounts: StoreSEPAAccounts;

    @action.bound public async importAll() {
        await Promise.all(this.all.map(credentials => this.sepaAccounts.finTSImport(credentials.id)));
    }

    @action.bound public async create(credentials: FinTSCredentials): Promise<FinTSCredentials> {
        return await super.create({
            ...credentials,
            user: { id: this.users.ownUser.id } as User,
        });
    }

    @initialize @action.bound protected async initialize() {
        const credentials = await this.controller.forUser(this.users.ownUser.id);
        credentials.forEach(current => this.entities.set(current.id, current));
    }
}
