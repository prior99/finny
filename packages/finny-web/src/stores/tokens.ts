import { Store } from "hyrest-mobx";
import { ControllerTokens } from "finny-common";
import { component, inject } from "tsdi";

@component
export class StoreTokens extends Store(ControllerTokens) {
    @inject protected controller: ControllerTokens;
}
