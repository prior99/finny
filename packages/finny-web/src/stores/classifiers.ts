import { ControllerClassifiers, Classifier } from "finny-common";
import { action, observable } from "mobx";
import { Store } from "hyrest-mobx";
import { inject, component, initialize } from "tsdi";
import { StoreUsers } from "./users";
import { StoreTransactions } from "./transactions";

@component
export class StoreClassifiers extends Store(ControllerClassifiers) {
    @inject protected controller: ControllerClassifiers;
    @inject private transactions: StoreTransactions;
    @inject private users: StoreUsers;

    @observable public loading = true;

    @initialize @action.bound public async initialize() {
        const classifiers = await this.controller.forUser(this.users.ownUser.id);
        classifiers.forEach(current => this.entities.set(current.id, current));
        this.loading = false;
    }

    @action.bound public async create(classifier: Classifier): Promise<Classifier> {
        const result = await super.create(classifier);
        await this.transactions.reload();
        return result;
    }

    public forTag(tag: string) {
        return this.all.filter(classifier => classifier.tag.id === tag);
    }

    @action.bound public deleteByTag(id: string) {
        this.entities.forEach(classifier => {
            if (classifier.tag.id !== id) { return; }
            this.entities.delete(classifier.id);
        });
    }
}
