import { ControllerSEPAAccounts } from "finny-common";
import { Store } from "hyrest-mobx";
import { action, observable } from "mobx";
import { inject, component, initialize } from "tsdi";
import { StoreUsers } from "./users";
import { StoreStatements } from "./statements";

@component
export class StoreSEPAAccounts extends Store(ControllerSEPAAccounts) {
    @inject protected controller: ControllerSEPAAccounts;
    @inject private users: StoreUsers;
    @inject private statements: StoreStatements;

    @observable private importing: string[] = [];

    @action.bound public async refreshAll() {
        await Promise.all(this.all.map(credentials => this.statements.refreshStatements(credentials.id)));
    }

    @initialize @action.bound protected async initialize() {
        const sepaAccounts = await this.controller.forUser(this.users.ownUser.id);
        sepaAccounts.forEach(current => this.entities.set(current.id, current));
    }

    public isImporting(id: string) {
        return this.importing.includes(id);
    }

    @action.bound public async finTSImport(credentialsId: string) {
        try {
            this.importing.push(credentialsId);
            const sepaAccounts = await this.controller.finTSImport(credentialsId);
            sepaAccounts.forEach(current => this.entities.set(current.id, current));
        } catch (error) {
            console.error(`Unable to import sepa accounts for FinTS credentials ${credentialsId}.`, error);
        }
        this.importing = this.importing.filter(current => current !== credentialsId);
    }
}
