import { action, observable } from "mobx";
import { ControllerTransactions, Transaction, TransactionSearchResult } from "finny-common";
import { component, inject } from "tsdi";
import { StoreUsers } from "./users";

@component
export class StoreTransactions {
    @inject protected controller: ControllerTransactions;
    @inject protected users: StoreUsers;

    @observable public currentResult: TransactionSearchResult;
    @observable public loading = false;
    @observable public currentQuery: [number, number, boolean, boolean];

    @action.bound public async reload() {
        await this.search(...this.currentQuery);
    }

    @action.bound public async search(
        page: number,
        limit: number,
        unclassified: boolean,
        hideInternal: boolean,
    ): Promise<TransactionSearchResult> {
        this.currentQuery = [page, limit, unclassified, hideInternal];
        this.loading = true;
        this.currentResult = await this.controller.search(
            this.users.ownUser.id,
            page,
            limit,
            unclassified,
            hideInternal,
        );
        this.loading = false;
        return this.currentResult;
    }
}
