import { ControllerClassifications } from "finny-common";
import { action } from "mobx";
import { Store } from "hyrest-mobx";
import { inject, component } from "tsdi";

@component
export class StoreClassifications extends Store(ControllerClassifications) {
    @inject protected controller: ControllerClassifications;

    @action.bound public async loadForClassifier(id: string) {
        const classifications = await this.controller.forClassifier(id);
        classifications.forEach(current => this.entities.set(current.id, current));
    }

    public forClassifier(id: string) {
        return this.all.filter(classification => classification.classifier.id === id);
    }

    public forTag(id: string) {
        return this.all.filter(classification => classification.tag.id === id);
    }
}
