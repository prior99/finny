import { ControllerStatements } from "finny-common";
import { Store } from "hyrest-mobx";
import { action, observable } from "mobx";
import { inject, component } from "tsdi";
import { StatementMonthlyAnalysis } from "finny-common";

@component
export class StoreStatements extends Store(ControllerStatements) {
    @inject protected controller: ControllerStatements;

    @observable private refreshing: string[] = [];

    @observable private monthlyAnalysis = new Map<string, StatementMonthlyAnalysis[]>();

    public isRefreshing(id: string) {
        return this.refreshing.includes(id);
    }

    @action.bound public async refreshStatements(sepaAccountId: string) {
        try {
            this.refreshing.push(sepaAccountId);
            const statements = await this.controller.refreshStatements(sepaAccountId);
            statements.forEach(current => this.entities.set(current.id, current));
        } catch (error) {
            console.error(`Unable to refresh statements for sepa account ${sepaAccountId}.`, error);
        }
        this.refreshing = this.refreshing.filter(current => current !== sepaAccountId);
    }

    @action.bound private async loadMonthlyAnalysis(sepaAccountId: string) {
        this.monthlyAnalysis.set(sepaAccountId, await this.controller.monthlyAnalysis(sepaAccountId));
    }

    public getMonthlyAnalysis(sepaAccountId: string) {
        if (!this.monthlyAnalysis.has(sepaAccountId)) {
            this.loadMonthlyAnalysis(sepaAccountId);
            return [];
        }
        return this.monthlyAnalysis.get(sepaAccountId);
    }
}
