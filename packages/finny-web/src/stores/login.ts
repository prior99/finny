import { component, inject, initialize } from "tsdi";
import { action, computed, observable } from "mobx";
import { pick } from "ramda";
import { User } from "finny-common";
import { StoreTokens } from "./tokens";

const softwareVersion = 2;
const localStorageIdentifier = "finny-login";

interface LocalStorageApi {
    readonly storageVersion: number;
    readonly date: string;
    readonly authToken: string;
    readonly userId: string;
}

@component({ eager: true })
export class StoreLogin {
    @inject private tokens: StoreTokens;

    @observable public authToken: string;
    @observable public userId: string;

    @initialize protected initialize() {
        const serialized = localStorage.getItem(localStorageIdentifier);
        if (serialized === null) { return; }
        let deserialized: LocalStorageApi;
        try {
            deserialized = JSON.parse(serialized);
            if (deserialized.storageVersion !== softwareVersion) {
                this.clearStorage();
                return;
            }
            this.authToken = deserialized.authToken;
            this.userId = deserialized.userId;
        } catch (err) {
            this.clearStorage();
            return;
        }
    }

    @action.bound public async login(user: User) {
        try {
            const token = await this.tokens.create({
                user: pick(["password", "email"], user) as User,
            });
            this.authToken = token.id;
            this.userId = token.user.id;
            this.save();
            location.reload();
            return true;
        } catch (error) {
            return false;
        }
    }

    @computed public get loggedIn() {
        return typeof this.authToken !== "undefined" && typeof this.userId !== "undefined";
    }

    @action.bound public logout() {
        this.clearStorage();
        this.authToken = undefined;
        this.userId = undefined;
        window.location.href = "/";
    }

    private save() {
        const deserialized: LocalStorageApi = {
            storageVersion: softwareVersion,
            date: new Date().toString(),
            authToken: this.authToken,
            userId: this.userId,
        };
        const serialized = JSON.stringify(deserialized);
        localStorage.setItem(localStorageIdentifier, serialized);
    }

    private clearStorage() {
        localStorage.removeItem(localStorageIdentifier);
    }
}
