import { ControllerTags, Tag, User, TagAnalysis } from "finny-common";
import { format } from "date-fns";
import { action, computed, observable } from "mobx";
import { Store } from "hyrest-mobx";
import { inject, component, initialize } from "tsdi";
import * as Color from "color";
import { StoreUsers } from "./users";
import { StoreClassifiers } from "./classifiers";
import { stringToColor } from "../string-to-color";

@component
export class StoreTags extends Store(ControllerTags) {
    @inject protected controller: ControllerTags;
    @inject private users: StoreUsers;
    @inject private classifiers: StoreClassifiers;

    @observable public loading = true;

    @initialize @action.bound public async initialize() {
        const tags = await this.controller.forUser(this.users.ownUser.id);
        tags.forEach(current => this.entities.set(current.id, current));
        this.loading = false;
    }

    @computed public get autoCompleteOptions() {
        return this.all.map(tag => ({
            value: tag.id,
            text: tag.name,
        }));
    }

    @action.bound public async create(tag: Tag): Promise<Tag> {
        return await super.create({
            ...tag,
            user: { id: this.users.ownUser.id } as User,
        });
    }

    public color(tagId: string): string {
        const tag = this.byId(tagId);
        if (!tag.parent) {
            return stringToColor(tag.id);
        }
        return Color(this.color(tag.parent.id)).lighten(0.3).toString();
    }

    public isChildOf(parentId: string, childId: string) {
        return this.allChildren(parentId).includes(childId);
    }

    public isParentOf(childId: string, parentId: string) {
        if (childId === parentId) { return true; }
        const child = this.byId(childId);
        if (!child.parent) { return false; }
        return this.isParentOf(child.parent.id, parentId);
    }

    public iterateChildren(tagId: string, method: (id: string) => void) {
        method(tagId);
        const tag = this.byId(tagId);
        this.all
            .filter(child => child.parent && child.parent.id === tagId)
            .map(child => child.id)
            .forEach(method);
    }

    public allChildren(tagId: string): string[] {
        const result: string[] = [tagId];
        this.iterateChildren(tagId, id => result.push(id));
        return result;
    }

    @action.bound public async delete(id: string): Promise<void> {
        this.classifiers.deleteByTag(id);
        await super.delete(id);
    }

    @action.bound public async analysis(
        sepaAccountId?: string,
        ignoreInternal?: boolean,
        month?: Date,
    ): Promise<TagAnalysis[]> {
        return await this.controller.analysis(
            this.users.ownUser.id,
            sepaAccountId,
            ignoreInternal,
            month && format(month, "YYYY-MM"),
        );
    }

    @computed public get rootTags() {
        return this.all.filter(tag => !tag.parent);
    }
}
