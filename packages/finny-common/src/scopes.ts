import { createScope } from "hyrest";

// General visibility.
export const ids = createScope();
export const world = createScope().include(ids);
export const owner = createScope().include(world);
// Users.
export const login = createScope();
export const signup = createScope().include(login);
export const updateUser = createScope();
// FinTS credentials.
export const createFinTSCredentials = createScope().include(ids);
export const getFinTSCredentials = createScope().include(ids);
// SEPA accounts.
export const getSEPAAccount = createScope().include(ids);
// Transactions.
export const getTransaction = createScope();
// Statements.
export const getStatement = createScope();
// Tags.
export const getTag = createScope().include(ids);
export const createTag = createScope().include(ids);
// Classifications.
export const getClassification = createScope().include(ids);
export const createClassification = createScope().include(ids);
// Classifiers.
export const getClassifier = createScope().include(ids);
export const createClassifier = createScope().include(ids);
