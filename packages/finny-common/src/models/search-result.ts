import { is, scope, specify, DataType } from "hyrest";
import { getTransaction } from "../scopes";
import { Transaction } from "./transaction";

export class TransactionSearchResult {
    @is() @scope(getTransaction) @specify(() => Transaction)
    public results?: Transaction[];

    @is() @scope(getTransaction)
    public page?: number;

    @is() @scope(getTransaction)
    public limit?: number;

    @is() @scope(getTransaction)
    public total?: number;
}
