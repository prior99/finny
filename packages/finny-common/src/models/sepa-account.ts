import { Column, PrimaryGeneratedColumn, Entity, ManyToOne, OneToMany } from "typeorm";
import { is, scope, specify, uuid } from "hyrest";
import { getSEPAAccount, ids } from "../scopes";
import { FinTSCredentials } from "./fints-credentials";
import { Statement } from "./statement";

@Entity()
export class SEPAAccount {
    @PrimaryGeneratedColumn("uuid")
    @is().validate(uuid) @scope(getSEPAAccount, ids)
    public id?: string;

    @Column("varchar", { length: 40 })
    @is() @scope(getSEPAAccount)
    public iban?: string;

    @Column("varchar", { length: 16 })
    @is() @scope(getSEPAAccount)
    public bic?: string;

    @Column("varchar")
    @is() @scope(getSEPAAccount)
    public accountNumber?: string;

    @Column("varchar")
    @is() @scope(getSEPAAccount)
    public subAccount?: string;

    @ManyToOne(() => FinTSCredentials, finTSCredentials => finTSCredentials.sepaAccounts)
    @is() @scope(getSEPAAccount) @specify(() => FinTSCredentials)
    public finTSCredentials?: FinTSCredentials;

    @OneToMany(() => Statement, statement => statement.sepaAccount)
    @is() @scope(getSEPAAccount) @specify(() => Statement)
    public statements?: Statement;

    public matches(other: Partial<SEPAAccount>) {
        return this.iban === other.iban && this.bic === other.bic;
    }
}
