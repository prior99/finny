import { Column, PrimaryGeneratedColumn, Entity, ManyToOne, OneToMany } from "typeorm";
import { PinTanClient } from "fints";
import { is, scope, specify, required, uuid, only } from "hyrest";
import { createFinTSCredentials, getFinTSCredentials, ids } from "../scopes";
import { blz as validateBlz } from "../validation";
import { User } from "./user";
import { SEPAAccount } from "./sepa-account";

@Entity()
export class FinTSCredentials {
    @PrimaryGeneratedColumn("uuid")
    @scope(getFinTSCredentials, ids) @is().validate(uuid)
    public id?: string;

    @Column("varchar", { length: 8 })
    @is().validate(validateBlz, only(createFinTSCredentials, required))
    @scope(getFinTSCredentials, createFinTSCredentials)
    public blz?: string;

    @Column("varchar", { length: 200 })
    @is().validate(only(createFinTSCredentials, required))
    @scope(getFinTSCredentials, createFinTSCredentials)
    public name?: string;

    @Column("varchar", { length: 200 })
    @is().validate(only(createFinTSCredentials, required))
    @scope(getFinTSCredentials, createFinTSCredentials)
    public url?: string;

    @Column("varchar", { length: 200 })
    @is().validate(only(createFinTSCredentials, required))
    @scope(getFinTSCredentials, createFinTSCredentials)
    public pin?: string;

    @ManyToOne(() => User, user => user.finTSCredentials)
    @is().validate(only(createFinTSCredentials, required))
    @scope(getFinTSCredentials, createFinTSCredentials) @specify(() => User)
    public user?: User;

    @OneToMany(() => SEPAAccount, sepaAccount => sepaAccount.finTSCredentials)
    @is() @scope(getFinTSCredentials) @specify(() => SEPAAccount)
    public sepaAccounts?: SEPAAccount[];

    public get client() {
        const { blz, pin, name, url } = this;
        return new PinTanClient({ blz, pin, name, url });
    }
}
