import { Column, PrimaryGeneratedColumn, Entity, ManyToOne, OneToMany } from "typeorm";
import { precompute, is, scope, specify, required, uuid, only } from "hyrest";
import { ids, getClassifier, createClassification, createClassifier } from "../scopes";
import { Tag } from "./tag";
import { Classification } from "./classification";
import { Transaction } from "./transaction";

@Entity()
export class Classifier {
    @PrimaryGeneratedColumn("uuid")
    @is().validate(uuid, only(createClassification, required))
    @scope(ids)
    public id?: string;

    @Column("varchar")
    @is() @scope(createClassifier, getClassifier)
    public iban?: string;

    @Column("varchar")
    @is() @scope(createClassifier, getClassifier)
    public nameRegexp?: string;

    @Column("varchar")
    @is() @scope(createClassifier, getClassifier)
    public textRegexp?: string;

    @Column("varchar")
    @is() @scope(createClassifier, getClassifier)
    public referenceRegexp?: string;

    @Column("decimal")
    @is() @scope(createClassifier, getClassifier)
    public thresholdMin?: number;

    @Column("decimal")
    @is() @scope(createClassifier, getClassifier)
    public thresholdMax?: number;

    @ManyToOne(() => Tag, tag => tag.classifiers)
    @is().validate(only(createClassification, required))
    @scope(createClassifier, getClassifier) @specify(() => Tag)
    public tag?: Tag;

    @OneToMany(() => Classification, classification => classification.classifier)
    @is() @scope(getClassifier) @specify(() => Classification)
    public classifications?: Classification[];

    @precompute @scope(getClassifier)
    public get classificationsCount(): number {
        if (!this.classifications) { return; }
        return this.classifications.length;
    }

    private testIBAN(transaction: Transaction) {
        if (typeof this.iban !== "string") { return true; }
        return this.iban === transaction.iban;
    }

    private testNameRegexp(transaction: Transaction) {
        if (typeof this.nameRegexp !== "string") { return true; }
        if (typeof transaction.name !== "string") { return false; }
        return Boolean(transaction.name.match(this.nameRegexp));
    }

    private testTextRegexp(transaction: Transaction) {
        if (typeof this.textRegexp !== "string") { return true; }
        if (typeof transaction.text !== "string") { return false; }
        return Boolean(transaction.text.match(this.textRegexp));
    }

    private testReferenceRegexp(transaction: Transaction) {
        if (typeof this.referenceRegexp !== "string") { return true; }
        if (typeof transaction.reference.raw !== "string") { return false; }
        return Boolean(transaction.reference.raw.match(this.referenceRegexp));
    }

    private testThresholdMin(transaction: Transaction) {
        if (typeof this.thresholdMin !== "number") { return true; }
        return Boolean(transaction.amount >= this.thresholdMin);
    }

    private testThresholdMax(transaction: Transaction) {
        if (typeof this.thresholdMax !== "number") { return true; }
        return Boolean(transaction.amount <= this.thresholdMax);
    }

    public test(transaction: Transaction) {
        return this.testIBAN(transaction) &&
            this.testNameRegexp(transaction) &&
            this.testTextRegexp(transaction) &&
            this.testReferenceRegexp(transaction) &&
            this.testThresholdMin(transaction) &&
            this.testThresholdMax(transaction);
    }

    public get name() {
        const conditions: { name: string, value: string }[] = [];
        if (this.iban) { conditions.push({ name: "IBAN", value: this.iban }); }
        if (this.nameRegexp) { conditions.push({ name: "Name", value: this.nameRegexp }); }
        if (this.textRegexp) { conditions.push({ name: "Text", value: this.textRegexp }); }
        if (this.referenceRegexp) { conditions.push({ name: "Reference", value: this.referenceRegexp }); }
        if (this.thresholdMin) { conditions.push({ name: "Minimal Amount", value: this.testThresholdMin.toString() }); }
        if (this.thresholdMax) { conditions.push({ name: "Maximum Amount", value: this.testThresholdMax.toString() }); }
        return conditions.map(({ name, value }) => `${name} = ${value}`).join(" and ");
    }
}
