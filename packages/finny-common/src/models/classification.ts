import { PrimaryGeneratedColumn, Entity, ManyToOne } from "typeorm";
import { is, scope, specify, required, uuid, only } from "hyrest";
import { ids, getClassification, createClassification } from "../scopes";
import { Tag } from "./tag";
import { Classifier } from "./classifier";
import { Transaction } from "./transaction";

@Entity()
export class Classification {
    @PrimaryGeneratedColumn("uuid")
    @is().validate(uuid) @scope(ids)
    public id?: string;

    @ManyToOne(() => Classifier, classifier => classifier.classifications)
    @is() @scope(getClassification, createClassification) @specify(() => Classifier)
    public classifier?: Classifier;

    @ManyToOne(() => Tag, tag => tag.classifications)
    @is().validate(only(createClassification, required))
    @scope(getClassification, createClassification) @specify(() => Tag)
    public tag?: Tag;

    @ManyToOne(() => Transaction, transaction => transaction.classifications)
    @is().validate(only(createClassification, required))
    @scope(getClassification, createClassification) @specify(() => Transaction)
    public transaction?: Transaction;
}
