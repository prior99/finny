import { Column, PrimaryGeneratedColumn, Entity, OneToMany } from "typeorm";
import { is, scope, specify, length, uuid, transform, only, required, precompute, email } from "hyrest";
import * as gravatar from "gravatar-url";
import { world, login, owner, signup, updateUser, createFinTSCredentials, ids, createTag } from "../scopes";
import { hash } from "../hash";
import { FinTSCredentials } from "./fints-credentials";
import { Token } from "./token";
import { Tag } from "./tag";

/**
 * A user from the database.
 */
@Entity()
export class User {
    @PrimaryGeneratedColumn("uuid")
    @is().validate(uuid, only(createTag, required), only(createFinTSCredentials, required))
    @scope(world, ids)
    public id?: string;

    @Column("varchar", { length: 100 })
    @is()
        .validate(length(3, 100), only(signup, required))
        .validateCtx(ctx => only(signup, value => ctx.validation.nameAvailable(value)))
    @scope(world, signup)
    public name?: string;

    @Column("varchar", { length: 200 })
    @transform(hash)
    @is().validate(
        length(8, 255),
        only(login, required),
        only(signup, required),
    )
    @scope(login, updateUser)
    public password?: string;

    @Column("varchar", { length: 200 })
    @is()
        .validate(
            length(8, 200),
            email,
            only(login, required),
            only(signup, required),
        )
        .validateCtx(ctx => only(signup, value => ctx.validation.emailAvailable(value)))
        .validateCtx(ctx => only(updateUser, value => ctx.validation.emailAvailable(value)))
    @scope(owner, login, updateUser)
    public email?: string;

    @OneToMany(() => Token, token => token.user)
    @is() @specify(() => Token) @scope(owner)
    public tokens?: Token[];

    @OneToMany(() => Token, token => token.user)
    @is() @specify(() => FinTSCredentials) @scope(owner)
    public finTSCredentials?: FinTSCredentials[];

    @precompute @scope(world)
    public get avatarUrl() {
        if (!this.email) { return; }
        return gravatar(this.email, { size: 200, default: "identicon" });
    }

    @Column("boolean", { default: false })
    @is() @scope(world, updateUser)
    public enabled?: boolean;

    @Column("boolean", { default: false })
    @is() @scope(world, updateUser)
    public admin?: boolean;

    @OneToMany(() => Tag, tag => tag.user)
    public tags: Tag[];
}
