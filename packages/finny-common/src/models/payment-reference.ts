import { is, scope, specify } from "hyrest";
import { getTransaction } from "../scopes";

export class PaymentReference {
    @is() @scope(getTransaction)
    public raw?: string;

    @is() @scope(getTransaction)
    public iban?: string;

    @is() @scope(getTransaction)
    public bic?: string;

    @is() @scope(getTransaction)
    public endToEndRef?: string;

    @is() @scope(getTransaction)
    public customerRef?: string;

    @is() @scope(getTransaction)
    public mandateRef?: string;

    @is() @scope(getTransaction)
    public creditorId?: string;

    @is() @scope(getTransaction)
    public originalTurnover?: string;

    @is() @scope(getTransaction)
    public interestCompensation?: string;

    @is() @scope(getTransaction)
    public divergingPrincipal?: string;

    @is() @scope(getTransaction)
    public bank?: string;

    @is() @scope(getTransaction)
    public back?: string;

    @is() @scope(getTransaction)
    public originatorId?: string;

    @is() @scope(getTransaction) @specify(() => Date)
    public date?: Date;

    @is() @scope(getTransaction)
    public text?: string;
}
