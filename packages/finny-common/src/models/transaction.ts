import { Column, PrimaryGeneratedColumn, Entity, ManyToOne, OneToMany } from "typeorm";
import { Transaction as FinTSTransaction }  from "fints";
import { DataType, is, scope, specify, required, uuid, only } from "hyrest";
import { PaymentReference } from "./payment-reference";
import { parse } from "date-fns";
import { ids, getTransaction, createClassification } from "../scopes";
import { Statement } from "./statement";
import { Classification } from "./classification";

@Entity()
export class Transaction {
    constructor();
    constructor(transaction: FinTSTransaction, statement: Statement);
    constructor(transaction?: FinTSTransaction, statement?: Statement) {
        if (!transaction) { return; }
        const { amount, isCredit, descriptionStructured } = transaction;
        this.code = transaction.code;
        this.fundsCode = transaction.fundsCode;
        this.currency = transaction.currency;
        this.description = transaction.description;
        this.amount = isCredit ? amount : -amount;
        this.valueDate = parse(transaction.valueDate);
        this.entryDate = parse(transaction.entryDate);
        this.customerReference = transaction.customerReference;
        this.bankReference = transaction.bankReference;
        this.statement = statement;
        this.name = descriptionStructured.name;
        this.iban = descriptionStructured.iban;
        this.text = descriptionStructured.text;
        this.bic = descriptionStructured.bic;
        this.primaNota = descriptionStructured.primaNota;
        this.reference = descriptionStructured.reference;
    }

    @PrimaryGeneratedColumn("uuid")
    @is().validate(uuid, only(createClassification, required))
    @scope(getTransaction, ids)
    public id?: string;

    @Column("varchar")
    @is() @scope(getTransaction)
    public code?: string;

    @Column("varchar")
    @is() @scope(getTransaction)
    public fundsCode?: string;

    @Column("varchar")
    @is() @scope(getTransaction)
    public currency?: string;

    @Column("text")
    @is() @scope(getTransaction)
    public description?: string;

    @Column("decimal")
    @is() @scope(getTransaction)
    public amount?: number;

    @Column("timestamp")
    @is() @scope(getTransaction) @specify(() => Date)
    public valueDate?: Date;

    @Column("timestamp")
    @is() @scope(getTransaction) @specify(() => Date)
    public entryDate?: Date;

    @Column("varchar")
    @is() @scope(getTransaction)
    public customerReference?: string;

    @Column("varchar")
    @is() @scope(getTransaction)
    public bankReference?: string;

    @ManyToOne(() => Statement, statement => statement.transactions)
    @is() @scope(getTransaction) @specify(() => Statement)
    public statement?: Statement;

    @Column("varchar")
    @is() @scope(getTransaction)
    public name?: string;

    @Column("varchar", { length: 40 })
    @is() @scope(getTransaction)
    public iban?: string;

    @Column("varchar")
    @is() @scope(getTransaction)
    public text?: string;

    @Column("varchar")
    @is() @scope(getTransaction)
    public bic?: string;

    @Column("varchar")
    @is() @scope(getTransaction)
    public primaNota?: string;

    @Column("jsonb")
    @is() @scope(getTransaction) @specify(() => PaymentReference)
    public reference?: PaymentReference;

    @OneToMany(() => Classification, classification => classification.transaction)
    @is() @scope(getTransaction) @specify(() => Classification)
    public classifications: Classification[];
}
