import { Column, PrimaryGeneratedColumn, Entity, ManyToOne, OneToMany } from "typeorm";
import { precompute, is, scope, specify, required, uuid, only } from "hyrest";
import { ids, getTag, createTag, createClassification, createClassifier } from "../scopes";
import { Classification } from "./classification";
import { Classifier } from "./classifier";
import { User } from "./user";

@Entity()
export class Tag {
    @PrimaryGeneratedColumn("uuid")
    @is().validate(uuid, only(createClassification, required), only(createClassifier, required))
    @scope(ids, createTag)
    public id?: string;

    @Column("varchar")
    @is() @scope(getTag, createTag)
    public name?: string;

    @ManyToOne(() => User, user => user.tags)
    @is() @scope(getTag, createTag)
    public user?: User;

    @ManyToOne(() => Tag, tag => tag.children)
    @is() @scope(getTag, createTag)
    public parent?: Tag;

    @OneToMany(() => Tag, tag => tag.parent)
    @is() @scope(getTag) @specify(() => Tag)
    public children?: Tag[];

    @OneToMany(() => Classification, classification => classification.tag)
    @is() @specify(() => Classification)
    public classifications?: Classification[];

    @OneToMany(() => Classifier, classifier => classifier.tag)
    @is() @scope(getTag) @specify(() => Classifier)
    public classifiers?: Classifier[];

    @precompute @scope(getTag)
    public get classificationsCount(): number {
        if (!this.classifications) { return; }
        return this.classifications.length;
    }
}
