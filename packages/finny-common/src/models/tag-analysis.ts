import { is, scope, specify } from "hyrest";
import { getTag } from "../scopes";
import { Tag } from "./tag";

export class TagAnalysis {
    @is() @scope(getTag)
    public tag?: Tag;

    @is() @scope(getTag) @specify(() => TagAnalysis)
    public children: TagAnalysis[];

    @is() @scope(getTag)
    public totalExpenses?: number;

    @is() @scope(getTag)
    public totalIncome?: number;

    public get exclusiveExpenses() {
        return this.totalExpenses - this.children.reduce((sum, child) => sum + child.totalExpenses, 0);
    }

    public get exclusiveIncome() {
        return this.totalIncome - this.children.reduce((sum, child) => sum + child.totalIncome, 0);
    }
}
