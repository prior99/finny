import { is, scope, specify } from "hyrest";
import { world } from "../scopes";

export class StatementMonthlyAnalysis {
    @is() @scope(world) @specify(() => Date)
    public month?: Date;

    @is() @scope(world)
    public minBalance?: number;

    @is() @scope(world)
    public maxBalance?: number;

    @is() @scope(world)
    public sumExpenses?: number;

    @is() @scope(world)
    public sumIncome?: number;

    public get profit() {
        return this.sumIncome - this.sumExpenses;
    }
}
