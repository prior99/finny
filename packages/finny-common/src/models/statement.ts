import { Column, PrimaryGeneratedColumn, Entity, ManyToOne, OneToMany } from "typeorm";
import { Statement as FinTSStatement }  from "fints";
import { is, scope, specify, uuid } from "hyrest";
import { parse } from "date-fns";
import { getStatement, ids } from "../scopes";
import { SEPAAccount } from "./sepa-account";
import { Transaction } from "./transaction";

@Entity()
export class Statement {
    constructor();
    constructor(statement: FinTSStatement, sepaAccount: SEPAAccount);
    constructor(statement?: FinTSStatement, sepaAccount?: SEPAAccount) {
        if (!statement) { return; }
        const { openingBalance, closingBalance } = statement;
        this.referenceNumber = statement.referenceNumber;
        this.relatedReferenceNumber = statement.relatedReferenceNumber;
        this.accountId = statement.accountId;
        this.number = statement.number;
        this.openingBalance = openingBalance.isCredit ? openingBalance.value : -openingBalance.value;
        this.closingBalance = closingBalance.isCredit ? closingBalance.value : -closingBalance.value;
        this.openingDate = parse(openingBalance.date);
        this.closingDate = parse(closingBalance.date);
        this.sepaAccount = sepaAccount;
    }

    @PrimaryGeneratedColumn("uuid")
    @is().validate(uuid) @scope(getStatement, ids)
    public id?: string;

    @Column("varchar")
    @is() @scope(getStatement)
    public referenceNumber?: string;

    @Column("varchar")
    @is() @scope(getStatement)
    public relatedReferenceNumber?: string;

    @Column("varchar")
    @is() @scope(getStatement)
    public accountId?: string;

    @Column("varchar")
    @is() @scope(getStatement)
    public number?: string;

    @Column("decimal")
    @is() @scope(getStatement)
    public openingBalance?: number;

    @Column("timestamp")
    @is() @scope(getStatement) @specify(() => Date)
    public openingDate?: Date;

    @Column("decimal")
    @is() @scope(getStatement)
    public closingBalance?: number;

    @Column("timestamp")
    @is() @scope(getStatement) @specify(() => Date)
    public closingDate?: Date;

    @ManyToOne(() => SEPAAccount, sepaAccount => sepaAccount.statements)
    @is() @scope(getStatement) @specify(() => SEPAAccount)
    public sepaAccount?: SEPAAccount;

    @OneToMany(() => Transaction, transaction => transaction.statement)
    @is() @scope(getStatement) @specify(() => Transaction)
    public transactions?: Transaction[];
}
