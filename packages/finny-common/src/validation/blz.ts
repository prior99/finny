import { Validation } from "hyrest";

export function blz(input: string): Validation {
    const num = Number(input);
    const valid = num >= 10000000 && num <= 89999999;
    return valid ? {} : { error: "Not a valid BLZ." };
}
