import {
    controller,
    route,
    created,
    unauthorized,
    context,
    param,
    notFound,
    ok,
    badGateway,
} from "hyrest";
import { warn } from "winston";
import { inject, component } from "tsdi";
import { Connection } from "typeorm";
import { Context } from "../context";
import { getSEPAAccount } from "../scopes";
import { User, SEPAAccount, FinTSCredentials } from "../models";

@controller @component
export class ControllerSEPAAccounts {
    @inject private db: Connection;

    @route("GET", "/sepa-account/:id").dump(SEPAAccount, getSEPAAccount)
    public async get(@param("id") id: string, @context ctx?: Context): Promise<SEPAAccount> {
        const sepaAccount = await this.db.getRepository(SEPAAccount).findOne({
            where: { id },
            relations: ["finTSCredentials", "finTSCredentials.user"],
        });
        if (!sepaAccount) {
            return notFound<SEPAAccount>("No such SEPA account.");
        }
        if (ctx && !await ctx.isUser(sepaAccount.finTSCredentials.user.id)) {
            return unauthorized<SEPAAccount>("Can't get SEPA account for foreign user.");
        }
        return ok(sepaAccount);
    }

    @route("GET", "/user/:id/sepa-accounts").dump(SEPAAccount, getSEPAAccount)
    public async forUser(@param("id") userId: string, @context ctx?: Context): Promise<SEPAAccount[]> {
        if (ctx && !await ctx.isUser(userId)) {
            return unauthorized<SEPAAccount[]>("Can't get SEPA account for foreign user.");
        }
        const user = await this.db.getRepository(User).findOne({ id: userId });
        if (!user) {
            return notFound<SEPAAccount[]>("No such user.");
        }
        const sepaAccounts = await this.db.getRepository(SEPAAccount).createQueryBuilder("sepaAccount")
            .leftJoinAndSelect("sepaAccount.finTSCredentials", "finTSCredentials")
            .leftJoinAndSelect("finTSCredentials.user", "user")
            .where("user.id = :userId", { userId })
            .getMany();
        return ok(sepaAccounts);
    }

    @route("POST", "/fints-credentials/:id/fints-import").dump(SEPAAccount, getSEPAAccount)
    public async finTSImport(@param("id") finTSCredentialsId: string, @context ctx?: Context): Promise<SEPAAccount[]> {
        const finTSCredentials = await this.db.getRepository(FinTSCredentials).findOne({
            where: { id: finTSCredentialsId },
            relations: ["user"],
        });
        if (!finTSCredentials) {
            return notFound<SEPAAccount[]>("No such FinTS credentials.");
        }
        const { client, user } = finTSCredentials;
        if (ctx && !await ctx.isUser(user.id)) {
            return unauthorized<SEPAAccount[]>("Can't import SEPA account for foreign user.");
        }
        const userAccounts = await this.forUser(user.id);
        try {
            const accounts = await client.accounts();
            const newAccounts = accounts.filter(account => {
                return !userAccounts.some(userAccount => userAccount.matches(account));
            });
            const imported = await Promise.all(newAccounts.map(async account => {
                const { iban, bic, accountNumber, subAccount } = account;
                const { id } = await this.db.getRepository(SEPAAccount).save({
                    iban, bic, accountNumber, subAccount, finTSCredentials,
                } as SEPAAccount);
                return this.get(id);
            }));
            return created(imported);
        } catch (error) {
            warn("Failed to import SEPA accounts via FinTS.");
            console.error(error);
            return badGateway<SEPAAccount[]>("Failed to import accounts.");
        }
    }
}
