import {
    controller,
    route,
    created,
    body,
    unauthorized,
    noauth,
    populate,
} from "hyrest";
import { inject, component } from "tsdi";
import { Connection } from "typeorm";
import { login, owner } from "../scopes";
import { User, Token } from "../models";

@controller @component
export class ControllerTokens {
    @inject private db: Connection;

    @route("POST", "/token").dump(Token, owner) @noauth
    public async create(@body(login) credentials: Token): Promise<Token> {
        const user = await this.db.getRepository(User).findOne(credentials);
        if (!user) {
            return unauthorized<Token>("Invalid credentials.");
        }
        if (!user.enabled) {
            return unauthorized<Token>("User is disabled.");
        }
        const newToken = await this.db.getRepository(Token).save({ user });
        return created(populate(Token, newToken));
    }
}
