import { controller, route, ok, body, noauth } from "hyrest";
import { inject, component } from "tsdi";
import { Connection } from "typeorm";
import { User, Token } from "../models";

interface ValidationResult {
    error?: string;
}

@controller @component
export class ControllerValidation {
    @inject private db: Connection;

    @route("POST", "/validate/user/name") @noauth
    public async nameAvailable(@body() name: string): Promise<ValidationResult> {
        const user = await this.db.getRepository(User).findOne({ name });
        if (user) {
            return ok({ error: "Name already taken." });
        }
        return ok({});
    }

    @route("POST", "/validate/user/email") @noauth
    public async emailAvailable(@body() email: string): Promise<ValidationResult> {
        const user = await this.db.getRepository(User).findOne({ email });
        if (user) {
            return ok({ error: "Email already taken." });
        }
        return ok({});
    }

    @route("POST", "/validate/token") @noauth
    public async tokenValid(@body() id: string): Promise<ValidationResult> {
        const token = await this.db.getRepository(Token).findOne({
            where: { id },
            relations: ["user"],
        });
        const valid = Boolean(token) && !token.deleted && token.user.enabled;
        if (!valid) {
            return ok({ error: "Invalid credentials." });
        }
        return ok({});
    }
}
