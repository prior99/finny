import {
    forbidden,
    context,
    is,
    controller,
    route,
    body,
    ok,
    notFound,
    param,
    uuid,
    badRequest,
} from "hyrest";
import { inject, component } from "tsdi";
import { Connection } from "typeorm";
import { Classification, Classifier, Transaction, Tag } from "../models";
import { getClassification, createClassification } from "../scopes";
import { Context } from "../context";

@component @controller
export class ControllerClassifications {
    @inject private db: Connection;

    @route("GET", "/classification/:id").dump(Classification, getClassification)
    public async get(@param("id") @is().validate(uuid) id: string, @context ctx?: Context) {
        const classification = await this.db.getRepository(Classification).findOne({
            where: { id },
            relations: ["tag", "transaction", "tag.user", "classifications"],
        });
        if (!classification) { return notFound<Classification>("No such classification."); }
        if (ctx && !await ctx.isUser(classification.tag.user.id)) {
            return forbidden<Classification>("Can't get classification for foreign user.");
        }
        return ok(classification);
    }

    @route("POST", "/classifications").dump(Classification, getClassification)
    public async create(@body(createClassification) classification: Classification, @context ctx?: Context) {
        const transaction = await this.db.getRepository(Transaction).findOne({
            where: { id: classification.transaction.id },
            relations: [
                "statement",
                "statement.sepaAccount",
                "statement.sepaAccount.finTSCredentials",
                "statement.sepaAccount.finTSCredentials.user",
            ],
        });
        if (!transaction) {
            return badRequest<Classification>("No such transaction.");
        }
        if (ctx && !await ctx.isUser(transaction.statement.sepaAccount.finTSCredentials.user.id)) {
            return forbidden<Classification>("Can't create classifiction for transaction of foreign user.");
        }
        const tag = await this.db.getRepository(Tag).findOne({
            where: { id: classification.tag.id },
            relations: ["user"],
        });
        if (!tag) {
            return badRequest<Classification>("No such tag.");
        }
        if (ctx && !await ctx.isUser(tag.user.id)) {
            return forbidden<Classification>("Can't create classifiction for tag of foreign user.");
        }
        const { id } = await this.db.getRepository(Classification).save(classification);
        return ok(await this.get(id));
    }

    @route("POST", "/classification/:id").dump(Classification, getClassification)
    public async update(
        @param("id") @is().validate(uuid) id: string,
        @body(createClassification) classification: Classification,
        @context ctx?: Context,
    ) {
        const oldClassification = await this.db.getRepository(Classification).findOne({
            where: { id },
            relations: ["tag", "tag.user"],
        });
        if (!oldClassification) { return notFound<Classification>("No such classification."); }
        if (ctx && !await ctx.isUser(oldClassification.tag.user.id)) {
            return forbidden<Classification>("Can't update classification for foreign user.");
        }
        await this.db.getRepository(Classification).update(id, classification);
        return ok(await this.get(id));
    }

    @route("GET", "/user/:id/classifications").dump(Classification, getClassification)
    public async forUser(@param("id") userId: string, @context ctx?: Context) {
        if (ctx && !ctx.isUser(userId)) {
            return forbidden<Classification[]>("Can't get classifications for foreign user.");
        }
        return ok(
            await this.db.getRepository(Classification).createQueryBuilder("classification")
                .leftJoinAndSelect("classification.tag", "tag")
                .leftJoinAndSelect("tag.user", "user")
                .leftJoinAndSelect("classification.transaction", "transaction")
                .leftJoinAndSelect("classification.classifier", "classifier")
                .where(`"user".id = :userId`, { userId })
                .getMany(),
        );
    }

    @route("GET", "/classifier/:id/classifications").dump(Classification, getClassification)
    public async forClassifier(@param("id") classifierId: string, @context ctx?: Context) {
        const classifier = await this.db.getRepository(Classifier).findOne({
            where: { id: classifierId },
            relations: ["tag", "tag.user"],
        });
        if (ctx && !ctx.isUser(classifier.tag.user.id)) {
            return forbidden<Classification[]>("Can't get classifications for foreign user.");
        }
        return ok(
            await this.db.getRepository(Classification).createQueryBuilder("classification")
                .leftJoinAndSelect("classification.tag", "tag")
                .leftJoinAndSelect("tag.user", "user")
                .leftJoinAndSelect("classification.transaction", "transaction")
                .leftJoinAndSelect("classification.classifier", "classifier")
                .where("classifier.id = :classifierId", { classifierId })
                .getMany(),
        );
    }
}
