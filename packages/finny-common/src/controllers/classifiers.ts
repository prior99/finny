import {
    is,
    controller,
    route,
    body,
    ok,
    notFound,
    param,
    uuid,
    context,
    forbidden,
    internalServerError,
} from "hyrest";
import { error } from "winston";
import { inject, component } from "tsdi";
import { Connection } from "typeorm";
import { Classifier, Transaction, Tag, Classification } from "../models";
import { getClassifier, createClassifier } from "../scopes";
import { Context } from "../context";

@component @controller
export class ControllerClassifiers {
    @inject private db: Connection;

    @route("GET", "/classifier/:id").dump(Classifier, getClassifier)
    public async get(@param("id") @is().validate(uuid) id: string, @context ctx?: Context) {
        const classifier = await this.db.getRepository(Classifier).findOne({
            where: { id },
            relations: ["tag", "classifications", "tag.user"],
        });
        if (!classifier) { return notFound<Classifier>("No such classifier."); }
        if (ctx && !await ctx.isUser(classifier.tag.user.id)) {
            return forbidden<Classifier>("Can't get classifier for foreign user.");
        }
        return ok(classifier);
    }

    @route("POST", "/classifiers").dump(Classifier, getClassifier)
    public async create(@body(createClassifier) classifier: Classifier, @context ctx?: Context) {
        const tag = await this.db.getRepository(Tag).findOne({
            where: { id: classifier.tag.id },
            relations: ["user", "classifications"],
        });
        if (ctx && !await ctx.isUser(tag.user.id)) {
            return forbidden<Classifier>("Can't create classifier for foreign user.");
        }
        try {
            const newId = await this.db.transaction(async db => {
                const newClassifier = await db.getRepository(Classifier).save(classifier);
                const transactions = await db.getRepository(Transaction).createQueryBuilder("transaction")
                    .leftJoinAndSelect("transaction.statement", "statement")
                    .leftJoinAndSelect("statement.sepaAccount", "sepaAccount")
                    .leftJoinAndSelect("sepaAccount.finTSCredentials", "finTSCredentials")
                    .leftJoinAndSelect("finTSCredentials.user", "user")
                    .where(`"user".id = :id`, { id: tag.user.id })
                    .getMany();
                await Promise.all(transactions.map(async transaction => {
                    if (newClassifier.test(transaction)) {
                        await db.getRepository(Classification).save({
                            classifier: { id: newClassifier.id },
                            transaction: { id: transaction.id },
                            tag: { id: tag.id },
                        });
                    }
                }));
                return newClassifier.id;
            });
            return ok(await this.get(newId));
        } catch (err) {
            error("Failed to create new classifier.");
            console.error(err);
            return internalServerError<Classifier>("Failed to create classifier.");
        }
    }

    @route("POST", "/classifier/:id").dump(Classifier, getClassifier)
    public async update(
        @param("id") @is().validate(uuid) id: string,
        @body(createClassifier) classifier: Classifier,
    ) {
        await this.db.getRepository(Classifier).update(id, classifier);
        return ok(await this.get(id));
    }

    @route("GET", "/user/:id/classifiers").dump(Classifier, getClassifier)
    public async forUser(@param("id") userId: string, @context ctx?: Context) {
        if (ctx && !await ctx.isUser(userId)) {
            return forbidden<Classifier[]>("Can't get classifier for foreign user.");
        }
        return ok(await this.db.getRepository(Classifier).find({
            where: { statement: { sepaAccount: { finTSCredentials: { user: { id: userId } } } } },
            relations: ["tag", "tag.user", "classifications"],
        }));
    }

    @route("DELETE", "/classifier/:id")
    public async delete(@param("id") @is().validate(uuid) id: string, @context ctx?: Context): Promise<void> {
        const classifier = await this.db.getRepository(Classifier).findOne({
            where: { id },
            relations: ["tag", "tag.user"],
        });
        if (!classifier) {
            return notFound<void>("No such classifier.");
        }
        if (ctx && !await ctx.isUser(classifier.tag.user.id)) {
            return forbidden<void>("Can't delete classifiers for foreign user.");
        }
        await this.db.transaction(async db => {
            await db.getRepository(Classification).createQueryBuilder("classification")
                .leftJoin("classification.classifier", "classifier")
                .where("classifier.id = :id", { id })
                .delete()
                .execute();
            await db.getRepository(Classifier).createQueryBuilder("classifier")
                .where("id = :id", { id })
                .delete()
                .execute();
        });
        return ok();
    }
}
