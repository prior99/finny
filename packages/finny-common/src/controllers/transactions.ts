import {
    controller,
    route,
    context,
    param,
    query,
    notFound,
    ok,
    forbidden,
    is,
    DataType,
    populate,
    dump,
} from "hyrest";
import { warn, info } from "winston";
import { inject, component } from "tsdi";
import { Connection } from "typeorm";
import { Context } from "../context";
import { getTransaction, world } from "../scopes";
import { PaymentReference, SEPAAccount, TransactionSearchResult, Transaction, Classification } from "../models";

@controller @component
export class ControllerTransactions {
    @inject private db: Connection;

    @route("GET", "/user/:id/transactions").dump(TransactionSearchResult, getTransaction)
    public async search(
        @param("id") userId: string,
        @query("page") @is(DataType.int) page = 0,
        @query("limit") @is(DataType.int) limit = 50,
        @query("unclassified") unclassified?: boolean,
        @query("hideInternal") hideInternal?: boolean,
        @context ctx?: Context,
    ): Promise<TransactionSearchResult> {
        if (ctx && !await ctx.isUser(userId)) {
            return forbidden<TransactionSearchResult>("Can't search for foreign user's transactions.");
        }
        const queryBuilder = this.db.getRepository(Transaction).createQueryBuilder("transaction")
            .leftJoinAndSelect("transaction.statement", "statement")
            .leftJoinAndSelect("transaction.classifications", "classifications")
            .leftJoinAndSelect("statement.sepaAccount", "sepaAccount")
            .leftJoinAndSelect("sepaAccount.finTSCredentials", "finTSCredentials")
            .leftJoinAndSelect("finTSCredentials.user", "user")
            .where(`"user".id = :userId`, { userId });
        if (hideInternal) {
            queryBuilder.andWhere(qb => {
                const subQuery = qb.subQuery()
                    .select("iban")
                    .from(SEPAAccount, "sepaAccount")
                    .leftJoin("sepaAccount.finTSCredentials", "finTSCredentials")
                    .where("finTSCredentials.user_id = :userId", { userId })
                    .getQuery();
                return `transaction.iban NOT IN (${subQuery})`;
            });
        }
        if (unclassified) {
            queryBuilder.andWhere(qb => {
                const subQuery = qb.subQuery()
                    .select("id")
                    .from(Classification, "classification")
                    .where("classification.transaction_id = transaction.id")
                    .getQuery();
                return `NOT EXISTS (${subQuery})`;
            });
        }
        const total = await queryBuilder.getCount();
        queryBuilder.skip(page * limit);
        queryBuilder.take(limit);
        return ok(populate(TransactionSearchResult, {
            results: await queryBuilder.getMany(),
            total,
            limit,
            page,
        }));
    }
}
