import {
    controller,
    route,
    created,
    unauthorized,
    context,
    param,
    notFound,
    ok,
    badGateway,
    populate,
    internalServerError,
} from "hyrest";
import { warn, info } from "winston";
import { inject, component } from "tsdi";
import { Connection } from "typeorm";
import { SEPAAccount as FinTSSEPAAccount, Statement as FinTSStatement } from "fints";
import { Context } from "../context";
import { getStatement, world } from "../scopes";
import {
    StatementMonthlyAnalysis,
    SEPAAccount,
    Statement,
    Transaction,
    Classifier,
    Classification,
} from "../models";

@controller @component
export class ControllerStatements {
    @inject private db: Connection;

    @route("GET", "/statement/:id").dump(Statement, getStatement)
    public async get(@param("id") id: string, @context ctx?: Context): Promise<Statement> {
        const statement = await this.db.getRepository(Statement).findOne({
            where: { id },
            relations: [
                "transaction",
                "sepaAccount",
                "sepaAccount.finTSCredentials",
                "sepaAccount.finTSCredentials.user",
            ],
        });
        if (!statement) {
            return notFound<Statement>("No such statement.");
        }
        const { sepaAccount } = statement;
        if (ctx && !await ctx.isUser(sepaAccount.finTSCredentials.user.id)) {
            return unauthorized<SEPAAccount>("Can't get statement for foreign user.");
        }
        return ok(statement);
    }

    @route("GET", "/sepa-account/:id/statements").dump(Statement, getStatement)
    public async forUser(@param("id") sepaAccountId: string, @context ctx?: Context): Promise<Statement[]> {
        const sepaAccount = await this.db.getRepository(SEPAAccount).findOne({
            where: { id: sepaAccountId },
            relations: ["finTSCredentials", "finTSCredentials.user"],
        });
        if (!sepaAccount) {
            return notFound<Statement[]>("No such SEPA account.");
        }
        if (ctx && !await ctx.isUser(sepaAccount.finTSCredentials.user.id)) {
            return unauthorized<Statement[]>("Can't get statements for foreign user.");
        }
        return await this.db.getRepository(Statement).createQueryBuilder("statement")
            .leftJoinAndSelect("statement.transactions", "transaction")
            .leftJoinAndSelect("statement.sepaAccount", "sepaAccount")
            .where("sepaAccount.id = :sepaAccountId", { sepaAccountId })
            .getMany();
    }

    @route("POST", "/sepa-account/:id/refresh-statements").dump(Statement, getStatement)
    public async refreshStatements(@param("id") sepaAccountId: string, @context ctx?: Context): Promise<Statement[]> {
        const newest = await this.db.getRepository(Statement).createQueryBuilder("statement")
            .orderBy("closing_date", "DESC")
            .leftJoinAndSelect("statement.sepaAccount", "sepaAccount")
            .where("sepaAccount.id = :sepaAccountId", { sepaAccountId })
            .getOne();
        const latestDate = newest ? newest.closingDate : new Date(0);
        const sepaAccount = await this.db.getRepository(SEPAAccount).findOne({
            where: { id: sepaAccountId },
            relations: ["finTSCredentials", "finTSCredentials.user"],
        });
        if (ctx && !await ctx.isUser(sepaAccount.finTSCredentials.user.id)) {
            return unauthorized<Statement[]>("Can't refresh statements for foreign user.");
        }
        const { client, blz } = sepaAccount.finTSCredentials;
        let rawStatements: FinTSStatement[];
        try {
            const account = { ...sepaAccount, blz } as FinTSSEPAAccount;
            rawStatements = await client.statements(account, latestDate, new Date());
        } catch (error) {
            warn("Failed to import statements from FinTS.");
            console.error(error);
            return badGateway<Statement[]>("Could not fetch statements from FinTS.");
        }
        try {
            const result: Statement[] = await this.db.transaction(async db => {
                const freshStatementPromises = rawStatements
                    .map(async (statement) => {
                        const {
                            openingDate,
                            closingDate,
                            openingBalance,
                            closingBalance,
                            accountId,
                        } = new Statement(statement, sepaAccount);
                        const exists = Boolean(await this.db.getRepository(Statement).findOne({
                            openingDate,
                            closingDate,
                            openingBalance,
                            closingBalance,
                            accountId,
                        }));
                        return { exists, statement };
                    });
                const freshStatements = (await Promise.all(freshStatementPromises))
                    .filter(({ exists }) => !exists)
                    .map(({ statement }) => statement);
                const classifiers = await db.getRepository(Classifier).createQueryBuilder("classifier")
                    .leftJoinAndSelect("classifier.tag", "tag")
                    .leftJoinAndSelect("tag.user", "user")
                    .where(`"user".id = :id`, { id: sepaAccount.finTSCredentials.user.id })
                    .getMany();
                return await Promise.all(freshStatements.map(async statement => {
                    const newStatement = await db.getRepository(Statement).save(new Statement(statement, sepaAccount));
                    await Promise.all(statement.transactions
                        .map(raw => new Transaction(raw, newStatement))
                        .map(async transaction => {
                            await Promise.all(classifiers.map(async classifier => {
                                if (classifier.test(transaction)) {
                                    await db.getRepository(Classification).save({
                                        classifier: { id: classifier.id },
                                        transaction: { id: transaction.id },
                                        tag: { id: classifier.tag.id },
                                    });
                                }
                            }));
                            return await db.getRepository(Transaction).save(transaction);
                        }));
                    return await db.getRepository(Statement).findOne({ id: newStatement.id });
                }));
            });
            info(`Imported ${result.length} new statements.`);
            return created(result);
        } catch (error) {
            error("Failed to store new statements in database.");
            console.error(error);
            return internalServerError<Statement[]>("Failed to store new statements in database.");
        }
    }

    @route("GET", "/sepa-account/:id/monthly-analysis").dump(StatementMonthlyAnalysis, world)
    public async monthlyAnalysis(
        @param("id") sepaAccountId: string,
        @context ctx?: Context,
    ): Promise<StatementMonthlyAnalysis[]> {
        const sepaAccount = await this.db.getRepository(SEPAAccount).findOne({
            where: { id: sepaAccountId },
            relations: ["finTSCredentials", "finTSCredentials.user"],
        });
        if (!sepaAccount) {
            return notFound<StatementMonthlyAnalysis[]>("No such SEPA account.");
        }
        if (ctx && !await ctx.isUser(sepaAccount.finTSCredentials.user.id)) {
            return unauthorized<StatementMonthlyAnalysis[]>("Can't get statements for foreign user.");
        }
        const rows = await this.db.getRepository(Statement).createQueryBuilder("statement")
            .select("SUM(GREATEST(closing_balance - opening_balance, 0))", "sumIncome")
            .addSelect("SUM(LEAST(closing_balance - opening_balance, 0)) * -1", "sumExpenses")
            .addSelect("DATE_TRUNC('month', closing_date)", "month")
            .addSelect("MAX(closing_balance)", "maxBalance")
            .addSelect("MIN(closing_balance)", "minBalance")
            .groupBy("DATE_TRUNC('month', closing_date)")
            .where("statement.sepa_account_id = :sepaAccountId", { sepaAccountId })
            .orderBy("month")
            .getRawMany();
        const result = rows
            .map(({ sumIncome, sumExpenses, month, maxBalance, minBalance }) => ({
                sumIncome: Number(sumIncome),
                sumExpenses: Number(sumExpenses),
                month,
                maxBalance: Number(maxBalance),
                minBalance: Number(minBalance),
            }))
            .map(populate(StatementMonthlyAnalysis));
        return ok(result);
    }
}
