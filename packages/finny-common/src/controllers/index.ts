export * from "./classifications";
export * from "./classifiers";
export * from "./fints-credentials";
export * from "./sepa-accounts";
export * from "./statements";
export * from "./tags";
export * from "./tokens";
export * from "./transactions";
export * from "./users";
export * from "./validation";

import { ControllerClassifications } from "./classifications";
import { ControllerClassifiers } from "./classifiers";
import { ControllerFinTSCredentials } from "./fints-credentials";
import { ControllerSEPAAccounts } from "./sepa-accounts";
import { ControllerStatements } from "./statements";
import { ControllerTags } from "./tags";
import { ControllerTokens } from "./tokens";
import { ControllerTransactions } from "./transactions";
import { ControllerUsers } from "./users";
import { ControllerValidation } from "./validation";

export const controllers: any[] = [
    ControllerClassifications,
    ControllerClassifiers,
    ControllerFinTSCredentials,
    ControllerSEPAAccounts,
    ControllerStatements,
    ControllerTags,
    ControllerTokens,
    ControllerTransactions,
    ControllerUsers,
    ControllerValidation,
];
