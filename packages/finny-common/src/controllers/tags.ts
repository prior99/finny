import {
    specify,
    query,
    populate,
    forbidden,
    context,
    is,
    controller,
    route,
    body,
    ok,
    notFound,
    param,
    uuid,
    DataType,
} from "hyrest";
import { parse } from "date-fns";
import { inject, component } from "tsdi";
import { Connection } from "typeorm";
import {
    Tag,
    Classification,
    Classifier,
    TagAnalysis,
    Transaction,
    Statement,
    SEPAAccount,
    FinTSCredentials,
} from "../models";
import { getTag, createTag } from "../scopes";
import { Context } from "../context";

interface AnalysisRow {
    id: string;
    parentId: string;
    expenseAmount: string;
    incomeAmount: string;
    children: string[];
}

@component @controller
export class ControllerTags {
    @inject private db: Connection;

    @route("GET", "/tag/:id").dump(Tag, getTag)
    public async get(@param("id") @is().validate(uuid) id: string, @context ctx?: Context) {
        const tag = await this.db.getRepository(Tag).findOne({
            where: { id },
            relations: ["classifications", "user", "parent"],
        });
        if (ctx && !await ctx.isUser(tag.user.id)) {
            return forbidden<Tag>("Can't get tag for foreign user.");
        }
        if (!tag) { return notFound<Tag>("No such tag."); }
        return ok(tag);
    }

    @route("POST", "/tags").dump(Tag, getTag)
    public async create(@body(createTag) tag: Tag, @context ctx?: Context) {
        if (ctx && !await ctx.isUser(tag.user.id)) {
            return forbidden<Tag>("Can't create tag for foreign user.");
        }
        const { id } = await this.db.getRepository(Tag).save(tag);
        return ok(await this.get(id));
    }

    @route("POST", "/tag/:id").dump(Tag, getTag)
    public async update(
        @param("id") @is().validate(uuid) id: string,
        @body(createTag) tag: Tag,
        @context ctx?: Context,
    ) {
        const oldTag = await this.db.getRepository(Tag).findOne({
            where: { id },
            relations: ["user", "classifications"],
        });
        if (ctx && !await ctx.isUser(oldTag.user.id)) {
            return forbidden<Tag>("Can't create tag for foreign user.");
        }
        await this.db.getRepository(Tag).update(id, tag);
        return ok(await this.get(id));
    }

    @route("GET", "/user/:id/tags").dump(Tag, getTag)
    public async forUser(@param("id") userId: string, @context ctx?: Context) {
        if (ctx && !await ctx.isUser(userId)) {
            return forbidden<Tag[]>("Can't get tags for foreign user.");
        }
        return ok(
            await this.db.getRepository(Tag).createQueryBuilder("tag")
                .leftJoinAndSelect("tag.user", "user")
                .leftJoinAndSelect("tag.parent", "parent")
                .leftJoinAndSelect("tag.classifications", "classifications")
                .where(`"user".id = :userId`, { userId })
                .getMany(),
        );
    }

    @route("DELETE", "/tag/:id")
    public async delete(@param("id") @is().validate(uuid) id: string, @context ctx?: Context): Promise<void> {
        const tag = await this.db.getRepository(Tag).findOne({
            where: { id },
            relations: ["user", "children"],
        });
        if (!tag) {
            return notFound<void>("No such tag.");
        }
        if (ctx && !await ctx.isUser(tag.user.id)) {
            return forbidden<void>("Can't delete tags for foreign user.");
        }
        await this.db.transaction(async db => {
            if (tag.children) {
                await Promise.all(tag.children.map(async child => {
                    await db.getRepository(Tag).update({ id: child.id}, { parent: null });
                }));
            }
            await db.getRepository(Classification).createQueryBuilder("classification")
                .leftJoin("classification.tag", "tag")
                .where("tag.id = :id", { id })
                .delete()
                .execute();
            await db.getRepository(Classifier).createQueryBuilder("classifier")
                .leftJoin("classifier.tag", "tag")
                .where("tag.id = :id", { id })
                .delete()
                .execute();
            await db.getRepository(Tag).createQueryBuilder("tag")
                .where("id = :id", { id })
                .delete()
                .execute();
        });
        return ok();
    }

    @route("GET", "/user/:userId/tags/analysis").dump(TagAnalysis, getTag)
    public async analysis(
        @param("userId") @is().validate(uuid) userId: string,
        @query("sepaAccount") @is().validate(uuid) sepaAccount?: string,
        @query("ignoreInternal") @is() ignoreInternal?: boolean,
        @query("month") @is() month?: string,
        @context ctx?: Context,
    ): Promise<TagAnalysis[]> {
        if (ctx && !await ctx.isUser(userId)) {
            return forbidden<TagAnalysis[]>("Can't get analysis for foreign user.");
        }
        const queryCTEStart = this.db.createQueryBuilder()
            .select("id", "id")
            .addSelect("ARRAY[]::UUID[]", "ancestors")
            .from(Tag, "tag")
            .where("parent_id IS NULL");
        const queryCTELoop = this.db.createQueryBuilder()
            .select("tag.id", "id")
            .addSelect("tree.ancestors || tag.parent_id", "ancestors")
            .from(Tag, "tag")
            .addFrom("tree", "tree")
            .where("tag.parent_id = tree.id");
        const queryCTE = `WITH RECURSIVE tree AS (${queryCTEStart.getSql()} UNION ALL ${queryCTELoop.getSql()})`;
        const queryUserIBANs = this.db.createQueryBuilder()
            .select("iban", "iban")
            .from(SEPAAccount, "sepaAccount")
            .leftJoin("sepaAccount.finTSCredentials", "finTSCredentials")
            .where("user_id = :userId", { userId });
        const queryGetTagged = this.db.createQueryBuilder()
            .select("parentTag.id", "id")
            .addSelect("parentTag.parent_id", "parentId")
            .addSelect(`SUM(LEAST("filteredTransaction".amount, 0)) * -1`, "expenseAmount")
            .addSelect(`SUM(GREATEST("filteredTransaction".amount, 0))`, "incomeAmount")
            .addSelect("ARRAY(SELECT id FROM tag WHERE parent_id = parentTag.id)", "children")
            .from(Tag, "parentTag")
            .leftJoin("tree", "tree", `"parentTag".id = ANY(tree.ancestors || tree.id)`)
            .leftJoin(Tag, "child_tag", "tree.id = child_tag.id")
            .leftJoin(Classification, "classification", "tag_id = child_tag.id")
            .leftJoin(subQueryBuilder => {
                const subQueryTransaction = subQueryBuilder
                    .select("transaction.id", "id")
                    .addSelect("transaction.amount", "amount")
                    .addSelect("transaction.iban", "iban")
                    .from(Transaction, "transaction")
                    .leftJoin("transaction.statement", "statement")
                    .leftJoin("statement.sepaAccount", "sepaAccount");
                if (ignoreInternal) {
                    subQueryBuilder.andWhere(() => `transaction.iban NOT IN (${queryUserIBANs.getSql()})`);
                }
                if (sepaAccount) {
                    subQueryBuilder.andWhere(`sepaAccount.id = :sepaAccount`);
                }
                if (month) {
                    subQueryBuilder.andWhere(
                        `DATE_TRUNC('month', transaction.value_date) = DATE_TRUNC('month', :month::timestamp)`,
                    );
                }
                return subQueryBuilder;
            }, "filteredTransaction", `classification.transaction_id = "filteredTransaction".id`)
            .where("parentTag.user_id = :userId")
            .groupBy("parentTag.id");
        const queryGetNotTagged = this.db.createQueryBuilder()
            .select("NULL", "id")
            .addSelect("NULL", "parentId")
            .addSelect("SUM(LEAST(amount, 0)) * -1", "expenseAmount")
            .addSelect("SUM(GREATEST(amount, 0))", "incomeAmount")
            .addSelect("ARRAY[]::UUID[]", "children")
            .from(Transaction, "transaction")
            .leftJoin("transaction.statement", "statement")
            .leftJoin("statement.sepaAccount", "sepaAccount")
            .where(subQueryFilterUntagged => {
                const subQueryAllClassifiedTransactions = this.db.createQueryBuilder()
                    .select("transaction_id", "classification")
                    .from(Classification, "classification");
                return `transaction.id NOT IN (${subQueryAllClassifiedTransactions.getSql()})`;
            });
        if (ignoreInternal) {
            queryGetNotTagged.andWhere(() => `transaction.iban NOT IN (${queryUserIBANs.getSql()})`);
        }
        if (sepaAccount) {
            queryGetNotTagged.andWhere(`sepaAccount.id = :sepaAccount`);
        }
        const queryFull = `${queryCTE} ${queryGetTagged.getSql()} UNION ${queryGetNotTagged.getSql()}`;
        const rows: AnalysisRow[] = await this.db.createQueryBuilder()
            .select("*")
            .from(`(${queryFull})`, "sq")
            .setParameters({
                sepaAccount,
                userId,
                month: parse(month),
            })
            .getRawMany();
        const createTagAnalysis = (row: AnalysisRow) => {
            return populate(TagAnalysis, {
                tag: row.id !== null ? { id: row.id } : undefined,
                totalExpenses: Number(row.expenseAmount),
                totalIncome: Number(row.incomeAmount),
                children: row.children.map(childId => {
                    const childRow = rows.find(current => current.id === childId);
                    return createTagAnalysis(childRow);
                }),
            });
        };
        const tagAnalysis = rows
            .filter(row => row.parentId === null)
            .map(createTagAnalysis);
        return ok(tagAnalysis);
    }
}
