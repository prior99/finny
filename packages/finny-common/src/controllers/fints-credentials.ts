import {
    controller,
    route,
    created,
    body,
    unauthorized,
    badRequest,
    context,
    param,
    notFound,
    ok,
    conflict,
} from "hyrest";
import { warn } from "winston";
import { inject, component } from "tsdi";
import { Connection } from "typeorm";
import { Context } from "../context";
import { createFinTSCredentials, getFinTSCredentials } from "../scopes";
import { User, FinTSCredentials } from "../models";

@controller @component
export class ControllerFinTSCredentials {
    @inject private db: Connection;

    @route("GET", "/fints-crendentials/:id").dump(FinTSCredentials, getFinTSCredentials)
    public async get(@param("id") id: string, @context ctx?: Context): Promise<FinTSCredentials> {
        const credentials = await this.db.getRepository(FinTSCredentials).findOne({
            where: { id },
            relations: ["user"],
        });
        if (!credentials) {
            return notFound<FinTSCredentials>("No such credentials.");
        }
        const { user } = credentials;
        if (ctx && !await ctx.isUser(user.id)) {
            return unauthorized<FinTSCredentials>("Can't get FinTS credentials for foreign user.");
        }
        return ok(credentials);
    }

    @route("GET", "/user/:userId/fints-credentials").dump(FinTSCredentials, getFinTSCredentials)
    public async forUser(@param("userId") userId: string, @context ctx?: Context): Promise<FinTSCredentials[]> {
        const user = await this.db.getRepository(User).findOne({ id: userId });
        if (!user) {
            return notFound<FinTSCredentials[]>("No such user.");
        }
        if (ctx && !await ctx.isUser(userId)) {
            return unauthorized<FinTSCredentials[]>("Can't get FinTS credentials for foreign user.");
        }
        const credentials = await this.db.getRepository(FinTSCredentials).find({
            where: { user: { id: userId } },
            relations: ["user"],
        });
        return credentials;
    }

    @route("POST", "/fints-credentials").dump(FinTSCredentials, getFinTSCredentials)
    public async create(
        @body(createFinTSCredentials) credentials: FinTSCredentials,
        @context ctx?: Context,
    ): Promise<FinTSCredentials> {
        const { user, blz, name } = credentials;
        if (ctx && !await ctx.isUser(user.id)) {
            return unauthorized<FinTSCredentials>("Can't add FinTS credentials for foreign user.");
        }
        const existing = await this.db.getRepository(FinTSCredentials).findOne({ where: { blz, name } });
        if (existing) {
            return conflict<FinTSCredentials>("Can't add the same credentials twice.");
        }
        const { client } = credentials;
        try {
            await client.accounts();
            const { id } = await this.db.getRepository(FinTSCredentials).save(credentials);
            return created(await this.get(id));
        } catch (error) {
            warn(`FinTS login unsuccessful.`);
            console.error(error);
            return badRequest<FinTSCredentials>("Invalid credentials.");
        }
    }
}
