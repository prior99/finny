import { User } from "./models";
import { ControllerValidation } from "./controllers";

export interface Context {
    validation: ControllerValidation;
    currentUser(): Promise<User>;
    isUser(id: string): Promise<boolean>;
}
